'use strict';

var gulp = require('gulp');
var util = require('gulp-util');
var async = require('async');

var Q = require('q');
var git = require('./dev_assets/git-rev');

gulp.paths = {
  src: 'src',
  dist: 'dist',
  tmp: '.tmp',
  e2e: 'e2e'
};

require('require-dir')('./gulp');

gulp.task('default', ['clean'], function () {
    gulp.start('build');
});

var awspublish = require('gulp-awspublish');

var localConfig = {
  buildSrc: './dist/**/*',
  getAwsConf: function (environment) {
    var environmentConfigurations = {
      'dev': {
          'accessKeyId': 'AKIAIRTF364G6GCOPYWA',
          'secretAccessKey': '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',
          'region': 'us-west-2',
          'params': {
              'Bucket': 'expo-registration'
          }
      },
      'production': {
          'accessKeyId': 'AKIAIRTF364G6GCOPYWA',
          'secretAccessKey': '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',
          'region': 'us-west-2',
          'params': {
             'Bucket': 'registration.expopass.com'
          }
      },
    };

    if (!environmentConfigurations[environment]) {
      throw 'No aws conf for env: ' + environment;
    }
    return { keys: environmentConfigurations[environment] };
  }
};

var getCurrentBranch = function() {
  var deferred = Q.defer();

  git.branch(function(error, branch) {
    if (error) {
      deferred.reject(error);
    } else {
      deferred.resolve(branch);
    }
  });

  return deferred.promise;

}

gulp.task('publish', ['build'], function() {
    getCurrentBranch().then(function(branch) {
      var currentBranch = branch;
      var requestedEnvironment = util.env.env;

      var awsConf = localConfig.getAwsConf(requestedEnvironment);

      var publisher = awspublish.create(awsConf.keys);

      if (requestedEnvironment === 'production' && currentBranch != 'master') {
        console.error('You are not allowed to publish to production from the current branch, which is ', currentBranch);
      } else {
        return gulp.src(localConfig.buildSrc)
            .pipe(awspublish.gzip({ ext: '' }))
            .pipe(publisher.publish(awsConf.headers))
            .pipe(publisher.cache())
            .pipe(publisher.sync())
            .pipe(awspublish.reporter());
      }
    }, function(error) {
      console.error(error);
    })

});
