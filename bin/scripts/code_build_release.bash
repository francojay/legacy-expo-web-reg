source venv/bin/activate

if [ "$CODEBUILD_BUILD_SUCCEEDING" = "1" ]; then
    VERSION="$(git rev-parse HEAD)"
    TIMESTAMP=`date "+%Y%m%d%H%M%S"`
    if [ "$CODEBUILD_WEBHOOK_HEAD_REF" = "refs/heads/master" ]; then
        echo "deploying to production_experimental"
        aws s3 cp dist s3://legacy-expo-reg-web --recursive --acl public-read
        aws s3 cp dist/index.html s3://legacy-expo-reg-web/index.html --metadata-directive REPLACE \
            --cache-control 'max-age=0, must-revalidate; no-cache' --content-type text/html --acl public-read
        aws s3 cp s3://legacy-expo-reg-web dist --recursive --acl public-read
        zip -r builds/expopass-frontend-reg-production-$VERSION.zip dist
        aws s3 cp builds/expopass-frontend-reg-production-$VERSION.zip s3://expopass-frontend-reg-artifacts/builds/expopass-frontend-reg-production--$TIMESTAMP-$VERSION.zip
        curl -X POST -H 'Content-type: application/json' --data '{"text":"`🎉 Continuous Deployment Success [Expo Web Reg] 🎉️`"}' https://hooks.slack.com/services/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
    elif [ "$CODEBUILD_WEBHOOK_HEAD_REF" = "refs/heads/develop" ]; then
        echo "deploying to staging_experimental"
        aws s3 cp dist s3://legacy-expo-reg-web --recursive --acl public-read
        aws s3 cp dist/index.html s3://legacy-expo-reg-web/index.html --metadata-directive REPLACE \
            --cache-control 'max-age=0, must-revalidate; no-cache' --content-type text/html --acl public-read
        aws s3 cp s3://legacy-expo-reg-web dist --recursive --acl public-read
        zip -r builds/expopass-frontend-reg-production-$VERSION.zip dist
        aws s3 cp builds/expopass-frontend-reg-production-$VERSION.zip s3://expopass-frontend-reg-artifacts/builds/expopass-frontend-reg-production--$TIMESTAMP-$VERSION.zip
        curl -X POST -H 'Content-type: application/json' --data '{"text":"`🎉 Continuous Deployment Success [Expo Web Reg] 🎉️`"}' https://hooks.slack.com/services/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
    else
        echo "could be worse"
    fi
fi
