require.register("urlConfigurations", function(exports, require, module) {
  var detectEnvironment = require("environmentDetector");
  var currentDetectedEnvironment = detectEnvironment();

  var configurator = {
  	"getBaseUrl": function() {
      if (currentDetectedEnvironment === 'DEV') return 'https://api-dev.expopass.com/api/v1.1/';
      else if (currentDetectedEnvironment === 'PROD') return 'https://api.expopass.com/api/v1.1/';
    },
    "getStripePublishableKey": function() {
    	if (currentDetectedEnvironment === 'DEV') return 'pk_test_jhZTPtSmu2SfMqTZ9EnGZvOo';
      else if (currentDetectedEnvironment === 'PROD') return 'pk_live_rpe80tCl1hjfTFORfbORdenw';
    }
  };

  module.exports = configurator;
});
