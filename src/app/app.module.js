(function() {
	'use strict'
	var servicesModule 		 = angular.module('expoform.services', []);
	var componentsModule	 = angular.module('expoform.components', []);

	var registrationModule = angular.module('expoform.registration', []);
	var expoTemplates 		 = angular.module('expoform.templates', []);

	var attendeeInfoModule = angular.module('expoform.registration.attendee_info', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var attendeeInfoModule = angular.module('expoform.registration.levels', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var sessionsModule 		 = angular.module('expoform.registration.sessions', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var guestsModule 			 = angular.module('expoform.registration.guests', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var cartModule 				 = angular.module('expoform.registration.cart', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var paymentModule 		 = angular.module('expoform.registration.payment', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var confirmationModule = angular.module('expoform.registration.confirmation', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var confirmationModule = angular.module('expoform.registration.finish', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var paymentFisModule 		 = angular.module('expoform.registration.paymentfis', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var registrationModules = [
		'expoform.registration.attendee_info',
		'expoform.registration.levels',
		'expoform.registration.sessions',
		'expoform.registration.guests',
		'expoform.registration.cart',
		'expoform.registration.payment',
		'expoform.registration.confirmation',
		'expoform.registration.finish',
		'expoform.registration.paymentfis'
	];

	angular.module("expoform.registration").requires = registrationModules;

	var preregistration 	 = angular.module('expoform.preregistration', []);

	var confirmModule  = angular.module('expoform.preregistration.confirm', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var preSessionsModule  = angular.module('expoform.preregistration.sessions', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var educationModule  = angular.module('expoform.preregistration.education', ['expoform.services', 'expoform.templates', 'expoform.components']);
	var finishModule  = angular.module('expoform.preregistration.finish', ['expoform.services', 'expoform.templates', 'expoform.components']);

	var preregistrationModules = [
			'expoform.preregistration.confirm',
			'expoform.preregistration.sessions',
			'expoform.preregistration.education',
			'expoform.preregistration.finish'
	];

	angular.module("expoform.preregistration").requires = preregistrationModules;

	var expoform					 = angular.module('expoform', ['ui.router', 'expoform.registration', 'expoform.preregistration']);
})();
