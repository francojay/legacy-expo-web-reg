(function(){
	'use strict'

	var directiveId 		= 'sessionsNavigationWarning';
	var app 						= angular.module("expoform.components");
	var depdenciesArray = [
		'$rootScope',
		'$state',
		sessionsNavigationWarning
	];

	app.directive(directiveId, depdenciesArray);

	function sessionsNavigationWarning($rootScope, $state) {
		return {
			restrict		: 'E',
			replace			: true,
			templateUrl : 'app/_components/sessionsNavigationWarning/sessionsNavigationWarningView.html',
			link				: link,
			scope 			: {
				visible : "=",
				text 	  : "=",
				icon		: "=",
				buttons : "="
			}
		};

		function link(scope) {
			console.log(scope)
		}
	}
})();
