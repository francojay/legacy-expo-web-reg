(function () {
  'use strict';

  var controllerId = 'preregistrationController';
	var module 			 = angular.module('expoform.registration');
  var dependencies = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    '_meta',
    preregistrationController
  ];
	module.controller(controllerId, dependencies);

  function preregistrationController($rootScope, $scope, $state, $stateParams, _meta) {
    $rootScope.preregistration = angular.copy(_meta);
    $rootScope.showLoader      = true;
  }
})();
