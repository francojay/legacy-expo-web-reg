(function(){
	'use strict'
  var serviceId		 = 'preregistrationFlowService';
	var module 			 = angular.module('expoform.services');
	var dependencies = [
		'requestService',
		'redrawService',
		'$q',
    '$state',
		preregistrationFlowService
	];

	module.service(serviceId, dependencies);

	function preregistrationFlowService(requestService, redrawService, $q, $state) {
		var service = {};

    service.sessions = [];
    service.educations = [];
		service.confirmPreregistration = function(token, confirmation) {
			redrawService.forceRedraw();
			var deferred = $q.defer();

			deferred.resolve();

			return deferred.promise;
		}

    service.saveSessions = function (sessions) {
			redrawService.forceRedraw();
			var deferred		 = $q.defer();
      service.sessions = sessions;
			deferred.resolve();
			return deferred.promise;
    }

    service.previousStepEducation = function () {
			redrawService.forceRedraw();
      $state.go('preregistration.sessions');
    }

    service.saveEducations = function (educations) {
			redrawService.forceRedraw();
			var deferred			 = $q.defer();
			service.educations = educations;

			deferred.resolve();
			return deferred.promise;
    }

		return service;
	}
})();
