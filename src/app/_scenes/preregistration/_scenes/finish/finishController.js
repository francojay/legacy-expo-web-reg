(function () {
  'use strict';

  var controllerId = 'educationController';
	var module 			 = angular.module('expoform.preregistration.sessions');
	var dependecies  = [
    '$rootScope',
    '$scope',
    'preregistrationFlowService',
		finishController
	]
	module.controller(controllerId, dependecies);

  function finishController($rootScope, $scope, preregistrationFlowService) {
    $scope.finishedLoadingData = true;
    $rootScope.showLoader      = false;

    $scope.previousStepEducation = function () {
      preregistrationFlowService.previousStepEducation();
    }

  }
})();
