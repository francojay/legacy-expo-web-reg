'use strict'
var finishModule		 	 = angular.module('expoform.preregistration.finish')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configFinish
];
finishModule.config(dependeciesArray);

function configFinish($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('preregistration.finish', {
		url: '/finish',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var preregitrationTicketId = $stateParams.ticket;

				return preregitrationTicketId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/preregistration/_scenes/finish/finishView.html',
				controller: 'educationController'
			}
		}
	})
}
