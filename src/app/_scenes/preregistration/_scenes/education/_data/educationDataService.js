(function(){
	'use strict'
  var serviceId		 = 'educationDataService';
	var module 			 = angular.module('expoform.preregistration.education');
	var dependencies = [
		'$q',
		'requestService',
		educationDataService
	];

	module.service(serviceId, dependencies);

	function educationDataService($q, requestService) {
		var service = {};

		service.getAllEducations = function(preregitrationTicketId) {
			var deferred = $q.defer()
			var url 		 = "preregistration/educations/";

			requestService.get(url, preregitrationTicketId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.saveEducations = function(cartId, educations) {
			var deferred = $q.defer();

			var url			 = "preregistration/educations/";
			var data     = {educations:educations}
			requestService.post(url, data, cartId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
