(function () {
  'use strict';

  var controllerId = 'educationController';
	var module 			 = angular.module('expoform.preregistration.sessions');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'educationDataService',
    'preregistrationFlowService',
		educationController
	]
	module.controller(controllerId, dependecies);

  function educationController($rootScope, $scope, $state, $stateParams, educationDataService, preregistrationFlowService) {
    var preregitrationTicketId = $rootScope.preregistration.preregitrationTicketId;
    var selectedEducations     = $rootScope.preregistration.educations;

    $scope.educations         = preregistrationFlowService.educations;
    $scope.selectedEducations = selectedEducations;

    if($scope.educations.length == 0) {
      educationDataService.getAllEducations(preregitrationTicketId).then(function(response) {
        $scope.educations  = response.data;
        initEducation();
      }, function(error) {
        console.log(error);
      });
    }
    else {
      initEducation();
    }

    $scope.selectEducation = function($event, education) {
      $event.stopPropagation();
      education.selected = !education.selected;
      if(education.selected) {
        $scope.selectedEducations.push(education.id);
      }
      else {
        $scope.selectedEducations.splice($scope.selectedEducations.indexOf(education.id),1);
      }
    }


    $scope.saveEducationsList = function () {
      $rootScope.showLoader   = true;
      educationDataService.saveEducations(preregitrationTicketId, $scope.selectedEducations).then(function(response) {
        preregistrationFlowService.saveEducations($scope.educations).then(function(response) {
          $scope.savedEducations = true;
          setTimeout(function () {
            $state.go('preregistration.finish');
          }, 200);
        });
      }, function(error) {
        console.log(error);
      })
    }

    $scope.previousStepEducation = function () {
      $rootScope.showLoader   = true;
      $rootScope.blurForm = true;
      setTimeout(function () {
        preregistrationFlowService.previousStepEducation();
      }, 200);

    }

    function initEducation() {
      for (var i = 0; i < $scope.educations.length; i++) {
        var currentEducation     = $scope.educations[i];
        for(var j = 0; j < $scope.selectedEducations.length; j++){
          if(currentEducation.id == $scope.selectedEducations[j]){
            currentEducation.selected = true;
            break;
          }
        }
      }
      $rootScope.showLoader = false;
      $scope.finishedLoadingData = true;
      $rootScope.blurForm = false;
    }

  }
})();
