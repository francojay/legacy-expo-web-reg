'use strict'
var educationModule		 	 = angular.module('expoform.preregistration.education')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configEducation
];
educationModule.config(dependeciesArray);

function configEducation($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('preregistration.education', {
		url: '/education',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var preregitrationTicketId = $stateParams.ticket;

				return preregitrationTicketId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/preregistration/_scenes/education/educationView.html',
				controller: 'educationController'
			}
		}
	})
}
