(function () {
  'use strict';

  var controllerId = 'preregistrationSessionsController';
	var module 			 = angular.module('expoform.preregistration.sessions');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'preregistrationSessionsDataService',
    'preregistrationFlowService',
		preregistrationSessionsController
	]
	module.controller(controllerId, dependecies);

  function preregistrationSessionsController($rootScope, $scope, $state, $stateParams, preregistrationSessionsDataService, preregistrationFlowService) {
    var preregitrationTicketId = $rootScope.preregistration.preregitrationTicketId;
    var conferenceStartDate    = $rootScope.preregistration.conferenceStartDate;
    var conferenceEndDate      = $rootScope.preregistration.conferenceEndDate;
    var conferenceTimezone     = $rootScope.preregistration.conferenceTimezone;
    var selectedSessions       = $rootScope.preregistration.sessions;

    var nextWasClickedAlready  = false;

    $scope.sessions         = preregistrationFlowService.sessions;
    $scope.groupedSessions  = [];
    $scope.selectedSessions = selectedSessions;

    if($scope.sessions.length == 0) {
      preregistrationSessionsDataService.getAllSessions(preregitrationTicketId).then(function(response) {
        $scope.sessions                           = response.data;
        if (response.data.length === 0) {
          $state.go('preregistration.education');
        } else {
          initSesssion();
        }
      }, function(error) {
        console.log(error);
      });
    }
    else {
      initSesssion();
    }

    $scope.selectGroup = function($event, group, index) {
      $event.stopPropagation();
      $scope.currentSessionGroup = {
        group : group,
        index : index
      };

      $scope.dropdownSelectorActive             = false;
      $scope.validGroupsExistBeforeCurrentGroup = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
      $scope.validGroupsExistAfterCurrentGroup  = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
      $scope.groupedSessions[index].wasActive   = true;
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null
    }

    $scope.toggleDropdownSelector = function() {
      $scope.dropdownSelectorActive = !$scope.dropdownSelectorActive;
    }

    $scope.selectPreviousGroup = function(currentGroup) {
      var momentStartDate = moment(conferenceStartDate).tz(conferenceTimezone);
      for (var day = currentGroup.group.day; day > momentStartDate; day.subtract(1, 'd')) {
        currentGroup.index--;
        var group = $scope.groupedSessions[currentGroup.index];
        if (Object.keys(group.sessionsByHour).length > 0) {
          $scope.currentSessionGroup = {
            group: group,
            index: currentGroup.index
          }
          $scope.validGroupsExistBeforeCurrentGroup            = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.validGroupsExistAfterCurrentGroup             = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.groupedSessions[currentGroup.index].wasActive = true;
          $scope.errorMessageTop                               = null;
          $scope.errorMessageBottom                            = null
          break;
        }
      }
    }

    $scope.selectNextGroup = function(currentGroup) {
      var momentEndDate = moment(conferenceEndDate).tz(conferenceTimezone);
      for (var day = currentGroup.group.day; day < momentEndDate; day.add(1, 'd')) {
        currentGroup.index++;
        var group = $scope.groupedSessions[currentGroup.index];
        if (Object.keys(group.sessionsByHour).length > 0) {
          $scope.currentSessionGroup = {
            group: group,
            index: currentGroup.index
          }
          $scope.validGroupsExistBeforeCurrentGroup       = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.validGroupsExistAfterCurrentGroup        = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.groupedSessions[currentGroup.index].wasActive = true;
          $scope.errorMessageTop                          = null;
          $scope.errorMessageBottom                       = null
          break;
        }
      }
    }

    $scope.selectSession = function($event, session) {
      $event.stopPropagation();
      session.selected = !session.selected;
      if(session.selected) {
        $scope.selectedSessions.push(session.session_id);
      }
      else {
        $scope.selectedSessions.splice($scope.selectedSessions.indexOf(session.session_id),1);
      }
    }

    $scope.openSessionDetails = function($event, session) {
      $event.stopPropagation();
      $rootScope.blurForm = true;
      $scope.openedSession = session;
      if (session.loaded) {
        $scope.openedSession = session;
      } else {
        preregistrationSessionsDataService.getOneSession( session.session_id, preregitrationTicketId).then(function(response) {
          session.description           = response.data.description;
          session.location              = response.data.location;
          session.continuing_educations = response.data.continuing_educations;
          session.ce_hours              = response.data.ce_hours;
          session.max_capacity          = response.data.max_capacity;
          session.session_type          = response.data.session_type;
          session.speakers              = response.data.speakers;

          session.loaded                = true;
          $scope.openedSession          = session;
        }, function(error) {
          console.log(error);
        })
      }
    }

    $scope.closeSessionDetails = function($event) {
      $event.stopPropagation();
      $scope.openedSession = null;
      $rootScope.blurForm = false;
    }

    $scope.saveSessionsList = function ($event, clickLocation) {
      $event.stopPropagation();
      $event.preventDefault();
      var allSessionDaysHaveBeenSeen = true;

      for (var i = 0; i < $scope.groupedSessions.length; i++) {
        allSessionDaysHaveBeenSeen = allSessionDaysHaveBeenSeen && $scope.groupedSessions[i].wasActive;
      }

      if (allSessionDaysHaveBeenSeen || nextWasClickedAlready) {
        $rootScope.showLoader   = true;
        $rootScope.blurForm = true;
        preregistrationSessionsDataService.saveSessions(preregitrationTicketId, $scope.selectedSessions).then(function(response) {
          preregistrationFlowService.saveSessions($scope.sessions).then(function(response) {
            $scope.sessionsSaved    = true;
            setTimeout(function () {
              $state.go('preregistration.education');
            }, 200);
          });
        }, function(error) {
          $rootScope.showLoader   = false;
          $rootScope.blurForm = false;
          if (clickLocation === 'bottom') {
            $scope.errorMessageBottom = error.data.detail;
          } else if (clickLocation === 'top') {
            $scope.errorMessageTop = error.data.detail;
          }
        })
      } else {
        if (clickLocation === 'bottom') {
          $scope.errorMessageBottom     = 'Oops, there are more days to view! Choose a new day from the list above to view more sessions.';
          $scope.dropdownSelectorActive = true;
        } else if (clickLocation === 'top') {
          $scope.errorMessageTop = 'Oops, there are more days to view! Choose a new day from the list to view more sessions.';
          $scope.dropdownSelectorActive = true;
        }
      }
      nextWasClickedAlready = true;
    }

    function initSesssion() {
      $scope.groupedSessions                    = groupSessionsByDay($scope.sessions, conferenceStartDate, conferenceEndDate, conferenceTimezone);
      $scope.currentSessionGroup                = initSelectedGroup($scope.groupedSessions);
      $scope.validGroupsExistBeforeCurrentGroup = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions)
      $scope.validGroupsExistAfterCurrentGroup  = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);

      $rootScope.showLoader                     = false;
      $scope.finishedLoadingData                = true;
      $rootScope.blurForm                       = false;
    }

    function groupSessionsByDay(sessions, conferenceStart, conferenceEnd, timezone) {
      var momentStartDate = moment(conferenceStart).tz(timezone);
      var momentEndDate   = moment(conferenceEnd).tz(timezone);
      var groupedSessions = [];
      for (var day = momentStartDate; day < momentEndDate; day.add(1, 'd')) {
        var dayCopy = angular.copy(day);
        var dayObject = {
          dayToDisplay  : day.format('dddd M/D'),
          day           : dayCopy,
          sessions      : [],
          wasActive     : false
        };

        for (var i = 0; i < sessions.length; i++) {
          var currentSession     = sessions[i];
          for(var j = 0; j < selectedSessions.length; j++){
            if(currentSession.session_id == selectedSessions[j]){
              currentSession.selected = true;
              break;
            }
          }
          var momentSessionStart = moment(currentSession.starts_at).tz(timezone);
          var momentSessionEnd   = moment(currentSession.ends_at).tz(timezone);
          if (momentSessionStart.format('YYYY/M/D') === momentStartDate.format('YYYY/M/D')) {
            dayObject.sessions.push(currentSession);
          }
        }

        if (dayObject.sessions.length > 0) {
          dayObject.sessionsByHour = {};
          for (var i = 0; i < dayObject.sessions.length; i++) {
            var currentSession     = dayObject.sessions[i];
            var momentSessionStart = moment(currentSession.starts_at).tz(timezone);
            var momentSessionEnd   = moment(currentSession.ends_at).tz(timezone);

            var groupStartKey      = momentSessionStart.format('h:00 A');
            var startHour          = momentSessionStart.hour();

            if (!dayObject.sessionsByHour[startHour]) {
              dayObject.sessionsByHour[startHour] = {
                keyToShowInView: groupStartKey,
                sessions       : []
              };
            }

            currentSession.formattedSessionStart = momentSessionStart.format('h:mm A');
            currentSession.formattedSessionEnd   = momentSessionEnd.format('h:mm A');
            currentSession.formattedDay          = momentSessionEnd.format('dddd M/D');
            dayObject.sessionsByHour[startHour].sessions.push(currentSession);
          }
        } else {
          dayObject.wasActive = true;
        }

        groupedSessions.push(dayObject);
      };
      return groupedSessions;
    }

    function initSelectedGroup(groupedSessions) {
      var currentMomentDay = moment();
      var currentGroup     = null;
      var currentIndex     = null;
      for (var i = 0; i < groupedSessions.length; i++) {
        if (groupedSessions[i].dayToDisplay === currentMomentDay.format('dddd M/D')) {
          currentGroup = groupedSessions[i];
          currentIndex = i;
          groupedSessions[i].wasActive = true;
          break
        }
      }

      if (currentGroup) {
        return {
          group: currentGroup,
          index: currentIndex
        }
      } else {
        for (var i = 0; i < groupedSessions.length; i++) {
          if (groupedSessions[i].sessions.length > 0) {
            currentGroup = groupedSessions[i];
            currentIndex = i;
            groupedSessions[i].wasActive = true;

            break
          }
        }
        return {
          group: currentGroup,
          index: currentIndex
        }
      }
    }

    function checkIfValidGroupsExistBeforeCurrentGroup(currentGroup, groups) {
      var currentGroupIndex = currentGroup.index;
      var currentGroup      = currentGroup.group;
      var foundValidGroup   = false;
      for (var i = currentGroupIndex - 1; i >= 0; i--) {
        if (groups[i].sessions.length > 0) {
          foundValidGroup = true;
          break
        }
      }

      return foundValidGroup;
    }

    function checkIfValidGroupsExistAfterCurrentGroup(currentGroup, groups) {
      var currentGroupIndex = currentGroup.index;
      var currentGroup      = currentGroup.group;
      var foundValidGroup   = false;
      for (var i = currentGroupIndex+1; i < groups.length; i++) {
        if (groups[i].sessions.length > 0) {
          foundValidGroup = true;
          break
        }
      }

      return foundValidGroup;
    }

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation()
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null;
    }
  }
})();
