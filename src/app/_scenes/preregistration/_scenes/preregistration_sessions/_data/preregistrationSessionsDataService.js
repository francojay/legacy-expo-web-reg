(function(){
	'use strict'
  var serviceId		 = 'preregistrationSessionsDataService';
	var module 			 = angular.module('expoform.preregistration.sessions');
	var dependencies = [
		'$q',
		'requestService',
		preregistrationSessionsDataService
	];

	module.service(serviceId, dependencies);

	function preregistrationSessionsDataService($q, requestService) {
		var service = {};

		service.getAllSessions = function(preregitrationTicketId) {
			var deferred = $q.defer()
			var url 		 = "preregistration/sessions/";

			requestService.get(url, preregitrationTicketId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.getOneSession = function(sessionId, cartId) {
			var deferred = $q.defer();

			var url			 = "preregistration/sessions/" + sessionId + "/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		service.saveSessions = function(cartId, sessions) {
			var deferred = $q.defer();

			var url			 = "preregistration/sessions/";
			var data     = {sessions:sessions}
			requestService.post(url, data, cartId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
