'use strict'
var preregistrationSessionsModule		 	 = angular.module('expoform.preregistration.sessions')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configPreregistrationSessions
];
preregistrationSessionsModule.config(dependeciesArray);

function configPreregistrationSessions($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('preregistration.sessions', {
		url: '/sessions',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var preregitrationTicketId = $stateParams.ticket;

				return preregitrationTicketId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/preregistration/_scenes/preregistration_sessions/preregistrationSessionsView.html',
				controller: 'preregistrationSessionsController'
			}
		}
	})
}
