'use strict'
var confirmModule		 		 = angular.module('expoform.preregistration.confirm')
var dependeciesArray 					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configConfirm
];
confirmModule.config(dependeciesArray);

function configConfirm($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('preregistration.confirm', {
		url: '/confirm',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
        //get metadata for pre-registration
        // var preregitrationTicketId = $stateParams.ticket;
        //
				// return preregitrationTicketId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/preregistration/_scenes/confirm/confirmView.html',
				controller: 'confirmController'
			}
		}
	})
}
