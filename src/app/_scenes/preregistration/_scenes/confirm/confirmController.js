(function () {
  'use strict';

  var controllerId = 'confirmController';
	var module 			 = angular.module('expoform.preregistration.confirm');
	var dependencies = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'preregistrationFlowService',
    '_meta',
    '$timeout',
		confirmController
	]
	module.controller(controllerId, dependencies);

  function confirmController($rootScope, $scope, $state, $stateParams, preregistrationFlowService, _meta, $timeout) {
    $rootScope.showLoader      = false;
    $scope.finishedLoadingData = true;
    $timeout(function() {
      var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 20;
      parent.postMessage(heightToSend, '*');
    }, 0);
    $scope.confirmPreregistration = function (confirmation) {
      preregistrationFlowService.confirmPreregistration(confirmation).then(function(result) {
        $scope.registrantInformationSaved = true;
        $rootScope.showLoader             = true;
        setTimeout(function () {
          $state.go('preregistration.sessions');
        }, 200);
      }, function(error) {
        console.log(error);
      });
    }

  }
})();
