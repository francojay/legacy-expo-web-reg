(function(){
	'use strict'

	var directiveId 		= 'cartShortcut';
	var app 						= angular.module("expoform.registration");
	var depdenciesArray = [
		'$rootScope',
		'$state',
		cartShortcut
	];

	app.directive(directiveId, depdenciesArray);

	function cartShortcut($rootScope, $state) {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'app/_scenes/registration/_components/cartShortcut/cartShortcutView.html',
			link: link
		};

		function link(scope) {
			var formId = $rootScope.registration.formId;
			var localStorageForms      = JSON.parse(localStorage.active_expoforms);
			for (var i = 0; i < localStorageForms.length; i++) {
				if (localStorageForms[i].formId === formId) {
					scope.cartLength 					 = localStorageForms[i].cart.length;
				}
			}

			scope.goToCart = function() {
				$rootScope.showLoader = true;
				$state.go('registration.cart');
			}
		}
	}
})();
