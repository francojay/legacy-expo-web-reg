/**
 * Dependency: http://glench.github.io/fuzzyset.js/
 * Version: 0.0.1a
 * Usage: <fuzz-email fclass="myclass" maxlength="100" emails="['gmail','yahoo','aol','hotmail','msn']" required></fuzz-email>
 */
 (function(){
 	 'use strict'

    var directiveId = 'fuzzyEmail';
    var app = angular.module('expoform.registration');
    var dependenciesArray = [
      fuzzyEmail
    ];

    app.directive(directiveId, dependenciesArray);

    function fuzzyEmail (){
      return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_scenes/registration/_components/email/fuzzyEmailView.html',
        link: link,
        scope: { field: '=', checkFunction: '&', modelOptions: '=' },
      }
      function link(scope,element,attrs) {
        var goodEmails = [
            "gmail.com",
            "yahoo.com",
            "aol.com",
            "hotmail.com"
        ]; //good_emails
        if (attrs.hasOwnProperty('emails')){
            goodEmails = eval(attrs.emails);
        }

        var fuzzyEmails = FuzzySet(goodEmails);

        scope.fuzzMe = function() {
          var email = scope.field.value;

          if (typeof email == 'undefined' || email == '') {
           scope.altEmail = false;
           return false;
          }

          var atpos = email.indexOf('@');
          var afterat = email.substring(atpos+1);
          //var dotpos = afterat.indexOf('.');
          if(atpos > 0 ) {
            var emailFirst = email.substring(0,atpos+1);
            //var emailDomain = afterat.substring(0,dotpos);
            //var emailLast = afterat.substring(dotpos);
            //console.log('first:'+ emailFirst + ' domain:' + emailDomain + ' last:' + emailLast);

            var fuzzy = fuzzyEmails.get(afterat);
            if ( (typeof fuzzy == 'undefined' || fuzzy == null ||  fuzzy == '')  ) {
              return false;
            }
            if (fuzzy != null && fuzzy.length && fuzzy[0][0] > 0.6 && fuzzy[0][0] < 1)  {
              scope.altEmail = emailFirst + fuzzy[0][1];
              //scope.altEmail   = true;
            }
            else {
              scope.altEmail = false;
            }
          }
          else{
            scope.altEmail = false;
          }

          return false;
        };
  		}
    };

})();
