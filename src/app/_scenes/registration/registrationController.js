(function () {
  'use strict';

  var controllerId = 'registrationController';
	var module 			 = angular.module('expoform.registration');
  var dependencies = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    '_meta',
    'registrantFlowService',
    registrationController
  ];
	module.controller(controllerId, dependencies);

  function registrationController($rootScope, $scope, $state, $stateParams, _meta, registrantFlowService) {
    if (_meta && _meta.formId) {
      $rootScope.registration = angular.copy(_meta);
      $rootScope.showLoader   = true;
      var formId              = _meta.formId;

      var localStorageForms   = JSON.parse(localStorage.active_expoforms);

      for (var i = 0; i < localStorageForms.length; i++) {
        if (localStorageForms[i].formId === formId) {
          if (localStorageForms[i].paymentDone === true) {
            if (localStorageForms[i].confirmationSent === true) {
              $state.go('registration.finish');
              break;
            } else {
              $state.go('registration.confirmation');
              break;
            }
          } else if (localStorageForms[i].cart.length > 0) {
            $state.go('registration.cart');
            break;
          } else {
            if (_meta.firstStep === 'attendee_first') {
              $state.go('registration.attendee_info');
              break;
            }
          }
        } else {
          if (_meta.firstStep === 'attendee_first') {
            $state.go('registration.attendee_info');
            break;
          }
        }
      };
    }

    $scope.closeAllTheDropdowns = function($event) {
      $rootScope.$broadcast("CLOSE_DROPDOWN");
    }
  }
})();
