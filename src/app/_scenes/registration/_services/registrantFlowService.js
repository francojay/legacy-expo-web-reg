(function(){
	'use strict'
  var serviceId		 = 'registrantFlowService';
	var module 			 = angular.module('expoform.services');
	var dependencies = [
		'requestService',
		'redrawService',
		'$q',
		'$state',
		'$rootScope',
		registrantFlowService
	];

	module.service(serviceId, dependencies);

	function registrantFlowService(requestService, redrawService, $q, $state, $rootScope) {
		var service = {};

		service.registrantObject = {
			"registrant"			: {
				"custom_fields"	: []
			},
			"guests"					: [],
			"addon_sessions"	: [],
			"level"						: 0
		};

		service.cartArray 			 = [];

		service.currentRegistrantLevelDetails = {
			"maximumNumberOfGuests"	: 0,
			"guestFee" 							: 0,
			"requireEmail"					: 0,
			"name"									: "",
			"price"									: 0,
			"guestBenefit" 					: "",
		}

		service.routesStack = [];

		service.tempGuests = [];

		service.setRegistrantInformation 	 = function(registrant) {
			var deferred = $q.defer();
			service.registrantObject.registrant.custom_fields = [];
			for (var i = 0; i < registrant.length; i++) {
				var registrantInformation = registrant[i];
				if (!registrantInformation.customFieldType) {
					if (registrantInformation.value) {
						service.registrantObject.registrant[registrantInformation.code] = registrantInformation.value;
					} else {
						service.registrantObject[registrantInformation.code] = undefined;
					}
				} else {
					if (registrantInformation.value) {
						var customField = {
							id: registrantInformation.code,
							value: registrantInformation.value.trim()
						}

						if (registrantInformation.customFieldType === 3) {
							var choicesArray = customField.value.split(",");
							customField.value = [];
							for (var j = 0; j < choicesArray.length; j++) {
								customField.value.push(choicesArray[j].trim());
							}
						}

						service.registrantObject.registrant.custom_fields.push(customField);
					}
				}
			}

			deferred.resolve();
			return deferred.promise;
		};

		service.setRegistrantLevel				 = function(level) {
			var deferred = $q.defer();

			service.registrantObject.level 															= level.id;

			service.currentRegistrantLevelDetails.maximumNumberOfGuests = level.nr_attendee_guests;
			service.currentRegistrantLevelDetails.guestFee 							= level.guest_fee;
			service.currentRegistrantLevelDetails.requireGuestEmail 		= level.require_guest_email;
			service.currentRegistrantLevelDetails.name 									= level.name;
			service.currentRegistrantLevelDetails.price 								= level.price;
			service.currentRegistrantLevelDetails.guestBenefit					= level.guest_benefit;

			deferred.resolve(service.registrantObject);

			return deferred.promise;
		};

		service.setRegistrantGuests 		 	 = function(guests) {
			var deferred = $q.defer();
			service.registrantObject.guests = [];
			for (var i = 0; i < guests.length; i++) {
				var guestObject = {
					"first_name"		: "",
					"last_name" 		: "",
					"email_address" : ""
				};

				for (var j = 0; j < guests[i].length; j++) {
					if (guests[i][j].code === 'first_name') {
						guestObject.first_name = guests[i][j].value;
					}

					if (guests[i][j].code === 'last_name') {
						guestObject.last_name = guests[i][j].value;
					}

					if (guests[i][j].code === 'email_address') {
						guestObject.email_address = guests[i][j].value;
					}
				}

				service.registrantObject.guests.push(guestObject);
			}

			deferred.resolve(service.registrantObject);
			return deferred.promise;
		};

		service.setRegistrantAddonSessions = function(groupedSessions) {
			var deferred = $q.defer();
			service.registrantObject.addon_sessions = [];
			for (var i = 0; i < groupedSessions.length; i++) {
				var group = groupedSessions[i];

				for (var j = 0; j < group.sessions.length; j++) {
					if (group.sessions[j].selected) {
						service.registrantObject.addon_sessions.push(group.sessions[j].session_id);
					}
				}
			}

			deferred.resolve(service.registrantObject);
			return deferred.promise;
		};

		service.saveCurrentRegistrant 		 = function(formId, cartId, registrant) {
			var deferred = $q.defer();
			var url 		 = 'forms/' + formId + '/registrants/';

			requestService.post(url, registrant, cartId).then(function(response) {
				service.registrantObject = {
					"registrant": {
						"custom_fields": []
					},
					"guests": [],
					"level": 0,
					"addon_sessions": []
				};
				service.cartArray 			 = [];

				service.currentRegistrantLevelDetails = {
					"maximumNumberOfGuests"	: 0,
					"guestFee" 							: 0,
					"requireEmail"					: 0
				};

				service.routesStack = [];

				deferred.resolve(response);

			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.saveRoute 								 = function(routeName) {
			redrawService.forceRedraw();
			service.routesStack.push(routeName);
		};

		service.navigateBack 							 = function() {
			service.routesStack 							= service.routesStack.filter(function(value, index, self) {
				return self.indexOf(value) === index;
			});

			if (service.routesStack.length > 1) {
				var currentActiveRoute 						= service.routesStack.pop();
				var previousActiveRoute						= service.routesStack[service.routesStack.length - 1] ? service.routesStack[service.routesStack.length - 1] : 'attendee_info';

				$rootScope.showLoader             = true;
				//service.unsetData(currentActiveRoute, previousActiveRoute);
				$state.go('registration.' + previousActiveRoute);
			}
		}

		service.unsetData									 = function(currentActiveRoute, previousActiveRoute) {
			if (currentActiveRoute === 'guests') {
				service.tempGuests = service.registrantObject.guests;
				service.registrantObject.guests = [];
			}

			if (currentActiveRoute === 'sessions') {
				service.registrantObject.addon_sessions = [];
			}

			if (currentActiveRoute === 'levels') {
				service.registrantObject.level = null
			}
		}

		service.getRoutesLength 					 = function() {
			service.routesStack 							= service.routesStack.filter(function(value, index, self) {
				return self.indexOf(value) === index;
			});
			return service.routesStack.length;
		}

		service.getFileContent						 = function(url) {
			var deferred = $q.defer();

			requestService.readFileContent(url).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		service.facebookTrackingEvent			 = function(page, eventObject) {
      if ($rootScope.registration.facebookTrackingCode) {
          var pass_data = {
              'func': 'facebook',
              'facebook_tracking_code': $rootScope.registration.facebookTrackingCode,
              'page': page,
              'event_object': eventObject
          };
          parent.postMessage(JSON.stringify(pass_data), "*");
      }
    }

    service.googleTrackingEvent 			 = function(obj) {
      if ($rootScope.registration.googleTrackingCode) {
          var pass_data = {
              'func'                : 'analytics',
              'google_tracking_code': $rootScope.registration.googleTrackingCode,
							'tracking_code'       : $rootScope.registration.googleTrackingCode, // for old integration
              'eventCategory'       : obj.eventCategory,
              'eventAction'         : obj.eventAction,
              'eventLabel'          : obj.eventLabel,
              'eventValue'          : obj.eventValue || null
          };
          parent.postMessage(JSON.stringify(pass_data), "*");
      }
    }

		service.savePartialCart 		 = function(formId, cartId) {
			var url 		 = 'forms/' + formId + '/cart/partial/';

			requestService.post(url, service.registrantObject, cartId);
		}

		return service;
	}
})();
