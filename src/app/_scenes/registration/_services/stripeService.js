(function(){
	'use strict'
  var serviceId		 = 'stripeService';
	var module 			 = angular.module('expoform.services');
	var dependencies = [
		'requestService',
    'API',
		stripeService
	];

	module.service(serviceId, dependencies);

	function stripeService(requestService, API) {
		var service = {};
    var formClass = '.payment-form' ;
    service.savedErrors = {};
    service.cardNumber = null;
    service.cardExpiry = null;
    service.cardCvc = null;

    var cardBrandToPfClass = {
      'visa': 'pf-visa',
      'mastercard': 'pf-mastercard',
      'amex': 'pf-american-express',
      'discover': 'pf-discover',
      'diners': 'pf-diners',
      'jcb': 'pf-jcb',
      'unknown': 'pf-credit-card',
    }

    service.initializeStripe =  function () {

      var stripe = Stripe(API.STRIPE_KEY);

      var elementStyles = {
        base: {
          fontFamily: 'Open Sans,Source Code Pro, Consolas, Menlo, monospace',
          fontSize: '16px'
        }
      };
      var elementClasses = {
        focus: 'focused',
        invalid: 'invalid'
      };
      var elements = stripe.elements();

      service.cardNumber = elements.create('cardNumber', {
        style: elementStyles,
        classes: elementClasses,
      });
      service.cardNumber.mount('#card-number');

      service.cardExpiry = elements.create('cardExpiry', {
        style: elementStyles,
        classes: elementClasses,
      });
      service.cardExpiry.mount('#card-expiry');

      service.cardCvc = elements.create('cardCvc', {
        style: elementStyles,
        classes: elementClasses,
      });
      service.cardCvc.mount('#card-cvc');
      registerElements([service.cardNumber, service.cardExpiry, service.cardCvc]);

      service.cardNumber.on('change', function(event) {
        if (event.brand) {
          setBrandIcon(event.brand);
        }

      });

      return stripe;
    }

   function registerElements (elements) {

      var example = document.querySelector(formClass);

      var form = example.querySelector('form');
      var error = example.querySelector('.error');
      var errorMessage = error.querySelector('.message');


      // Listen for errors from each Element, and show error messages in the UI.
      service.savedErrors = [];
      elements.forEach(function(element, idx) {
        element.on('change', function(event) {
          if (event.error) {
            error.classList.add('visible');
            service.savedErrors[idx] = event.error.message;
            errorMessage.innerText = event.error.message;
          } else {
            service.savedErrors[idx] = null;

            // Loop over the saved errors and find the first one, if any.
            var nextError = Object.keys(service.savedErrors)
              .sort()
              .reduce(function(maybeFoundError, key) {
                return maybeFoundError || service.savedErrors[key];
              }, null);

            if (nextError) {
              // Now that they've fixed the current error, show another one.
              errorMessage.innerText = nextError;
            } else {
              // The user fixed the last error; no more errors.
              error.classList.remove('visible');
            }
          }
        });
      });

    }
    service.checkErrorsStripeElements = function (errors){
      var example = document.querySelector(formClass);
      var error = example.querySelector('.error');
      var errorMessage = error.querySelector('.message');
       error.classList.add('visible');
      //service.savedErrors[idx] = errors.error.message;
      errorMessage.innerText = errors.error.message;
    }

    function setBrandIcon (brand) {
    	var brandIconElement = document.getElementsByClassName('brand');
      var pfClass = 'pf-credit-card';
      if (brand in cardBrandToPfClass) {
      	pfClass = cardBrandToPfClass[brand];
      }
      for (var i = brandIconElement.length - 1; i >= 0; i--) {
				brandIconElement[i].querySelector('i').classList.remove('active');
      }
			for (var i = brandIconElement.length - 1; i >= 0; i--) {
				if(brandIconElement[i].querySelector('i').classList.value.indexOf(pfClass) >= 0){
					brandIconElement[i].querySelector('i').classList.add('active');
				}
      }
      // brandIconElement.classList.add('pf');
      // brandIconElement.classList.add(pfClass);
    }
		return service;
	}
})();
