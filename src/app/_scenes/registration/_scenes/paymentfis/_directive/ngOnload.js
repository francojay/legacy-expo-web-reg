(function(){
	 'use strict'

   var directiveId = 'ngOnload';
   var app = angular.module('expoform.registration.paymentfis');

   var dependenciesArray = [
     '$rootScope',
     ngOnloadDirective
   ];

   app.directive(directiveId, dependenciesArray);

		function ngOnloadDirective ($rootScope){
			return{
        restrict: "A",
        scope: {
            callBack: '&ngOnload'
        },
        link: function(scope, element, attrs){
           $rootScope.showLoader = true;
            element.on('load', function(){
              var contentLocation = element.length > 0 && element[0].contentWindow ? element[0].contentWindow.location : undefined;
                return scope.callBack({
                            contentLocation: contentLocation
                        });
            })
        }
			}
		};
})();
