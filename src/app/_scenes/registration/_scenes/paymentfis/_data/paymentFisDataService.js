(function(){
	'use strict'
  var serviceId		 = 'paymentFisDataService';
	var module 			 = angular.module('expoform.registration.paymentfis');
	var dependencies = [
		'$q',
		'requestService',
		paymentFisDataService
	];

	module.service(serviceId, dependencies);

	function paymentFisDataService($q, requestService) {
		var service = {};

		service.getFISHPPage = function(formId, cartId, cartData) {
			var deferred = $q.defer();
			var url 		 = 'forms/'+ formId +'/fis/form/';

			var orderObject = {
				"return_url": cartData.returnUrl,
				"payment_mode": cartData.paymentMode
			};

			requestService.post(url, orderObject, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		service.completeFisPayment = function(formId, cartId) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/fis/result/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};


		return service;
	}
})();
