(function() {
  'use strict';

  var controllerId = 'paymentFisController';

  var module = angular.module('expoform.registration.paymentfis');
  var dependecies = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    'paymentFisDataService',
    '$sce',
    '$window',
    'paymentDataService',
    'registrantFlowService',
    paymentFisController
  ]
  module.controller(controllerId, dependecies);

  function paymentFisController($rootScope, $scope, $state, $stateParams, paymentFisDataService, $sce, $window, paymentDataService, registrantFlowService) {

    var formId = $rootScope.registration.formId;
    var cartId = $rootScope.registration.cartId;

    if ($stateParams.order_code !== '') {
      $scope.paymentInProgress = true;
      $rootScope.showLoader = true;
      registrantFlowService.facebookTrackingEvent('Expo_Pay', {event: $stateParams.id, value: $scope.totalToPay});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_PayEC','eventLabel': $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_PayPrice','eventLabel': String($scope.totalToPay)});
      completeFisPayment();
    } else if ($state.params.totalToPay <= 0) {
      $scope.paymentInProgress = true;
      $rootScope.showLoader = true;
      completeFisPayment(true);
    } else {
      getHPPage();
    }

    parent.postMessage({'task': 'scroll_top'}, '*');
    
    function getHPPage() {
      registrantFlowService.facebookTrackingEvent('Expo_CCard', {event: $stateParams.id, value: $scope.totalToPay});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CCard','eventLabel': $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CCardPrice','eventLabel': String($scope.totalToPay)});
      var order = {};
      order.returnUrl = "https://" + window.location.hostname + "/#!/registration/" + $stateParams.id + "/payment-fis/y";
      order.paymentMode = 'P';
      paymentFisDataService.getFISHPPage(formId, cartId, order).then(function(response) {
        $scope.consumerUrl = $sce.trustAsResourceUrl(response.consumer_url);

      });
      setTimeout(function() {
        $scope.paymentInProgress = false;
        $scope.finishedLoadingData = true;
        $rootScope.showLoader = false;
      }, 200);
    }

    $scope.handleFrameLoaded = function(contentLocation) {
      //console.log(contentLocation);
      if (contentLocation.hash && contentLocation.hash != "") {
        $rootScope.showLoader = true;
        $scope.paymentInProgress = true;
        if (contentLocation.href.indexOf('/y/') > -1) {
          var order_code = contentLocation.href;
          order_code = order_code.substring(order_code.indexOf("y/") + 2, order_code.length);
          $state.go('registration.paymentfis', {
            order_code: order_code
          });
        }
      }
    }

    function completeFisPayment(freeCart) {
      if (!freeCart) {
        paymentFisDataService.completeFisPayment(formId, cartId).then(function(response) {
          if (response.response_code == "11") {
            $state.go('registration.cart');
          } else if (response.response_code == "1") {
            $scope.paymentDone = true;
            var localStorageForms = JSON.parse(localStorage.active_expoforms);
            for (var i = 0; i < localStorageForms.length; i++) {
              if (localStorageForms[i].formId === formId) {
                localStorageForms[i].paymentDone = true;
              }
            };
            localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

            setTimeout(function() {
              $state.go('registration.confirmation');
            }, 200);
          } else {
            $rootScope.errorMessage = "An error occurred processing your card. Please, feel free to contact the organizer or try to another card.";
            getHPPage();
          }
        }, function(error) {
          $state.go('registration.cart')
        });
      }
      else {
        paymentDataService.completePayment(formId, cartId, null).then(function(response) {
          var localStorageForms = JSON.parse(localStorage.active_expoforms);
          for (var i = 0; i < localStorageForms.length; i++) {
            if (localStorageForms[i].formId === formId) {
              localStorageForms[i].paymentDone = true;
            }
          };
          localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

          setTimeout(function() {
            $rootScope.loaderUserMessage = null;
            $state.go('registration.confirmation');
          }, 200);
        }, function(error) {
          if (error.error_type == 'cart') {
            $state.go('registration.cart', {
              errors: error.detail
            });
          }
          $scope.cardError = error.detail;
          $scope.paymentDone = false;
          $scope.paymentInProgress = false;
          $rootScope.showLoader = false;
          $rootScope.loaderUserMessage = null;
        });
      }
    }

  }
})();
