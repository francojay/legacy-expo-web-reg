'use strict'
var paymentModule		 		 = angular.module('expoform.registration.paymentfis')
var dependeciesArray 					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configPaymentFis
];
paymentModule.config(dependeciesArray);

function configPaymentFis($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.paymentfis', {
		url: '/payment-fis',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				console.log($stateParams.id)
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/paymentfis/paymentFisView.html',
				controller: 'paymentFisController'
			}
		},
		params: {
      totalToPay: null,
			order_code:""
    }
	})
	// .state('registration.completepaymentfis', {
	// 	url: '/payment-fis/:redir/:order_code',
	// 	resolve: {
	// 		_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
	// 			console.log($stateParams.id)
	// 			//get metadata for registration form
	// 			var registrationFormId = $stateParams.id;
	//
	// 			return registrationFormId;
	// 		}]
	// 	},
	// 	views: {
	// 		"form": {
	// 			templateUrl: 'app/_scenes/registration/_scenes/paymentfis/paymentFisView.html',
	// 			controller: 'paymentFisController'
	// 		}
	// 	},
	// 	params: {
  //     totalToPay: null,
	// 		redir:"",
	// 		order_code:""
  //   }
	// })
}
