(function(){
	'use strict'
  var serviceId		 = 'guestsDataService';
	var module 			 = angular.module('expoform.registration.guests');
	var dependencies = [
		'requestService',
		guestsDataService
	];

	module.service(serviceId, dependencies);

	function guestsDataService(requestService) {
		var service = {};

		service.guests_test = function() {
			return "guests_test";
		}

		return service;
	}
})();
