
(function () {
  'use strict';

  var controllerId = 'guestsController';
	var module 			 = angular.module('expoform.registration.guests');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'guestsDataService',
    'registrantFlowService',
    '$timeout',
		guestsController
	]
	module.controller(controllerId, dependecies);

  function guestsController($rootScope, $scope, $state, $stateParams, guestsDataService, registrantFlowService, $timeout) {
    var formId         = $rootScope.registration.formId;
    var cartId         = $rootScope.registration.cartId;

    var currentRegistrant             = registrantFlowService.registrantObject;
    var currentRegistrantLevelDetails = registrantFlowService.currentRegistrantLevelDetails;
    var tempGuests                    = registrantFlowService.tempGuests;
    $scope.guestFee                   = currentRegistrantLevelDetails.guestFee;
    $scope.numberOfGuests             = currentRegistrantLevelDetails.maximumNumberOfGuests;
    var requireGuestEmail             = currentRegistrantLevelDetails.requireGuestEmail;

    var genericGuestObject = [
      {
        name        : "First Name",
        code        : "first_name",
        visibility  : "required",
        value       : ""
      },
      {
        name        : "Last Name",
        code        : "last_name",
        visibility  : "required",
        value       : ""
      },
      {
        name        : "Email Address",
        code        : "email_address",
        visibility  : requireGuestEmail ? "required" : 'optional',
        value       : ""
      }
    ];

    $scope.attendeeGuests   = [];
    $scope.canGoToNextStep  = true;
    $rootScope.showLoader   = false;
    registrantFlowService.saveRoute('guests');

    if ($scope.numberOfGuests === 0) {
      $rootScope.showLoader = true;
      registrantFlowService.saveCurrentRegistrant(formId, cartId, currentRegistrant).then(function(response) {
        $state.go('registration.cart');
      }, function(error) {
        if(error.data.error_type == "session") {
          $state.go('registration.sessions', { errors:error.data.detail });
          registrantFlowService.routesStack.pop();
        } else if(error.data.error_type == 'attendee') {
          $state.go('registration.attendee_info', { errors:error.data.detail });
        }
      });
    } else {
      $scope.listOfOptions    = createGuestsDropdownOptions($scope.guestFee, $scope.numberOfGuests);
      $scope.selectedOption   = $scope.listOfOptions[0];
      if(tempGuests.length > 0) {
        $scope.selectedOption = $scope.listOfOptions[tempGuests.length];
        createAttendeeGuestsObjects(true);
        validateTempGuests();
        registrantFlowService.facebookTrackingEvent('Expo_Guest', {event: $stateParams.id});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_GuestEC','eventLabel': $stateParams.id});
      }
    }

    parent.postMessage({'task': 'scroll_top'}, '*');
    $timeout(function() {
      var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
      parent.postMessage(heightToSend, '*');
    }, 0);
    $scope.selectNumberOfGuests = function(option) {
      $scope.selectedOption  = option;
      $scope.canGoToNextStep = option.number > 0 ? false : true;

      registrantFlowService.facebookTrackingEvent('Expo_GuestCount', {event: $stateParams.id, Guest_Count: option.number});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_GuestEC','eventLabel': $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_GuestCountEC','eventLabel': String(option.number)});
      createAttendeeGuestsObjects();

      $scope.checkIfCanEnableNextButton();
      if(document.getElementsByClassName('_body').length > 0) {
        document.getElementsByClassName('_body')[0].setAttribute('style','height:auto');
      }
      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.toggleDropdownSelector = function($event) {
      $event.stopPropagation();
      $event.preventDefault();
      $scope.dropdownSelectorActive = !$scope.dropdownSelectorActive;

      var heightToSend = document.getElementsByClassName('_footer')[0].getBoundingClientRect().top;
      //console.log(heightToSend)
      var dropdown     = $event.target.getBoundingClientRect().top + 45 + 166;
      //console.log(dropdown)
      if (dropdown > heightToSend && $scope.dropdownSelectorActive) {
        document.getElementsByClassName('_body')[0].setAttribute('style','height:'+dropdown+'px');
      }
      else{
        document.getElementsByClassName('_body')[0].setAttribute('style','height:auto');
      }
      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.checkIfNeedsValidation = function(field) {
      if (field.code === 'email_address') {
        field = validateEmailField(field);
      }
      $scope.checkIfCanEnableNextButton();
    }

    $scope.checkIfCanEnableNextButton = function() {
      var canGo = true;
      for (var i = 0; i < $scope.attendeeGuests.length; i++) {
        var field = $scope.attendeeGuests[i];
        for (var j = 0; j < field.length; j++) {
          var visibility = field[j].visibility;
          var value      = field[j].value;
          var error      = field[j].error;

          if (error) {
            canGo = false;
            break
          } else if (visibility === 'required' && value === "") {
            canGo = false;
            break;
          }
        }
      }

      $scope.canGoToNextStep = canGo;
    }

    $scope.saveRegistrantGuests = function(guests, clickLocation) {
      if ($scope.canGoToNextStep) {
        registrantFlowService.setRegistrantGuests(guests).then(function(response) {
          $scope.registrantInformationSaved = true;
          $rootScope.showLoader             = true;
          registrantFlowService.saveCurrentRegistrant(formId, cartId, currentRegistrant).then(function(response) {
            $state.go('registration.cart');
          }, function(error) {
            $rootScope.showLoader             = false;
            $scope.registrantInformationSaved = false;

            if (error.data.error_type === "attendee") {
              $state.go('registration.attendee_info', { errors: error.data.detail });
            } else if (error.data.error_type == "session") {
              $state.go('registration.sessions', { errors: error.data.detail });
            } else {
              if (clickLocation === 'top') {
                $scope.errorMessageTop = error.data.detail;
              } else if (clickLocation === 'bottom') {
                $scope.errorMessageBottom = error.data.detail;
              }
            }
          });
          registrantFlowService.savePartialCart(formId, cartId);
        }, function(error) {
          console.log(error);
        })
      } else {
        if (clickLocation === 'top') {
          $scope.errorMessageTop = "Please complete all the required fields."
        } else if (clickLocation === 'bottom') {
          $scope.errorMessageBottom = "Please complete all the required fields."
        }
      }
    }

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation()
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null;
    }

    $scope.navigateBack = function() {
      registrantFlowService.setRegistrantGuests($scope.attendeeGuests).then(function(response) {
        $scope.registrantInformationSaved = true;
        $rootScope.showLoader             = true;
      }, function(error) {
        console.log(error);
      })
      registrantFlowService.unsetData('guests');
      $scope.previousButtonDisabled = true;
      registrantFlowService.navigateBack();
    }

    function validateEmailField(field) {
      var address = field.value;
      if (address.length > 0) {
        var regexExpression = /[a-zA-Z0-9+_-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
        var match =   address.match(regexExpression);
        field.error = !regexExpression.test(address) || match.toString() != address ;
      }
      return field;
    }

    function createGuestsDropdownOptions(guestFee, numberOfGuests) {
      var guestsOptionsArray = [{
          label: 'No guests',
          number: 0
      }];

      for (var i = 1; i <= numberOfGuests; i++) {
          guestsOptionsArray.push({
              label  :i > 1 ? i + ' Guests' : i + ' Guest',
              price  : guestFee * i,
              number : i
          })
      }

      $scope.finishedLoadingData = true;
      return guestsOptionsArray;
    }

    function createAttendeeGuestsObjects(autocomplete) {
        var i = 0;
        var numberOfCompletedGuests = $scope.selectedOption.number;
        $scope.attendeeGuests       = $scope.attendeeGuests ? $scope.attendeeGuests : [];
        var currentGuestsLength = $scope.attendeeGuests.length;
        if(numberOfCompletedGuests > currentGuestsLength) {
        while (i < numberOfCompletedGuests - currentGuestsLength) {
            if(!autocomplete){
              var copyGenericGuestObject = angular.copy(genericGuestObject);
              for(j = 0; j < copyGenericGuestObject.length; j ++){
                copyGenericGuestObject[j].key = copyGenericGuestObject[j].code + '_' + i;
              }
              $scope.attendeeGuests.push(copyGenericGuestObject);
            }
            else{
              var completedGuests = angular.copy(genericGuestObject);
              for (var j = 0; j < completedGuests.length; j++) {
                completedGuests[j].value = tempGuests[i][completedGuests[j].code];

              }
              $scope.attendeeGuests.push(completedGuests);
            }
            i++;
        }
      }
      else if(numberOfCompletedGuests < currentGuestsLength){
        while (i < currentGuestsLength - numberOfCompletedGuests) {
            $scope.attendeeGuests.splice($scope.attendeeGuests.length-1,1);
            i++;
        }
      }

      $scope.canGoToNextStep = false;
    }

    $rootScope.$on("CLOSE_DROPDOWN", function(event, data) {
      $scope.dropdownSelectorActive = false;
      if(document.getElementsByClassName('_body').length > 0) {
        document.getElementsByClassName('_body')[0].setAttribute('style','height:auto');
      }
      $timeout(function() {
       var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
       parent.postMessage(heightToSend, '*');
      }, 0);
    });

    function validateTempGuests() {
      var canGo = true;
      for (var i = 0; i < $scope.attendeeGuests.length; i++) {
        var field = $scope.attendeeGuests[i];
        for (var j = 0; j < field.length; j++) {
          if (field[j].code === 'email_address') {
            field[j] = validateEmailField(field[j]);
          }
          var visibility = field[j].visibility;
          var value      = field[j].value;
          var error      = field[j].error;

          if (error) {
            canGo = false;
            break
          } else if (visibility === 'required' && value === "") {
            canGo = false;
            break;
          }
        }
      }

      $scope.canGoToNextStep = canGo;
    }
  }
})();
