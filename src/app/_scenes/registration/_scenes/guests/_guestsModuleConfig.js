'use strict'
var guestsModule		 		 = angular.module('expoform.registration.guests')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configGuests
];
guestsModule.config(dependeciesArray);

function configGuests($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.guests', {
		url: '/guests',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', 'registrantFlowService', '$state', function($rootScope, $http, $stateParams, registrantFlowService, $state) {
				var currentRegistrant  = registrantFlowService.registrantObject;
				var formId 						 = new Hashids('Expo', 7).decode($stateParams.id)[0];
				var localStorageForms  = JSON.parse(localStorage.active_expoforms);

				if (!currentRegistrant.registrant.first_name && !currentRegistrant.registrant.last_name && !currentRegistrant.registrant.email_address) {
					for (var i = 0; i < localStorageForms.length; i++) {
			      if (localStorageForms[i].formId === formId) {
							if (localStorageForms[i].firstStep === 'attendee_first') {
								$state.go('registration.attendee_info', {id: $stateParams.id});
							} else if (localStorageForms[i].firstStep === 'level_firsts') {
								$state.go('registration.levels', {id: $stateParams.id});
							}
						}
			    };
				}
				return currentRegistrant;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/guests/guestsView.html',
				controller: 'guestsController'
			}
		}
	})
}
