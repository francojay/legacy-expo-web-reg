'use strict'
var cartModule		 			 = angular.module('expoform.registration.cart')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configCart
];
cartModule.config(dependeciesArray);

function configCart($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.cart', {
		url: '/cart',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/cart/cartView.html',
				controller: 'cartController'
			}
		},
		params: {
      errors: null
    }
	})
}
