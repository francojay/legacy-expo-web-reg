(function(){
	'use strict'
  var serviceId		 = 'cartDataService';
	var module 			 = angular.module('expoform.registration.cart');
	var dependencies = [
		'$q',
		'requestService',
		cartDataService
	];

	module.service(serviceId, dependencies);

	function cartDataService($q, requestService) {
		var service = {};

		service.getCartItems = function(formId, cartId) {
			var deferred = $q.defer();
			var url 	   = "forms/" + formId + "/cart/"

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data)
			});

			return deferred.promise;
		};

		service.removeRegistrantFromCart = function(formId, cartId, registrant) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/registrants/" + registrant.id + "/";

			requestService.delete(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		service.removePromoCode = function(formId, cartId, registrant) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/promo_codes/" + registrant.id + "/";

			requestService.delete(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(response.data);
			});

			return deferred.promise;
		};

		service.applyPromoCode = function(formId, cartId, promoCode) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/promo_codes/";

			var promoCodeObject = {
				"code": promoCode
			};

			requestService.post(url, promoCodeObject, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		service.removePromoCode = function(formId, cartId, registrantId) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/promo_codes/" + registrantId + "/";

			requestService.delete(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		return service;
	}
})();
