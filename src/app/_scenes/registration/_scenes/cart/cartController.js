(function () {
  'use strict';

  var controllerId = 'cartController';
	var module 			 = angular.module('expoform.registration.cart');
	var dependecies  = [
    '$q',
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'cartDataService',
    '$timeout',
    'registrantFlowService',
		cartController
	]
	module.controller(controllerId, dependecies);

  function cartController($q, $rootScope, $scope, $state, $stateParams, cartDataService, $timeout, registrantFlowService) {
    var formId     = $rootScope.registration.formId;
    var cartId     = $rootScope.registration.cartId;


    $scope.routesLength = registrantFlowService.getRoutesLength();
    $scope.foundErrors  = false;

    if ($scope.routesLength > 0) {
      registrantFlowService.saveRoute('cart');
      $scope.routesLength++;
    }


    $scope.registrantToBeRemoved = null;
    $scope.promoCodeToBeRemoved  = null;
    $scope.promoCodeModel        = null;
    $scope.promoCodeError        = null;
    $scope.promoCodeSuccess      = null;
    $scope.cart                  = null;

    getCart(formId, cartId);
    parent.postMessage({'task': 'scroll_top'}, '*');
    
    if($stateParams.errors != null){
      $scope.errorMessageTop = $stateParams.errors;
    }

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation();
      $scope.errorMessageTop    = false;
      $scope.errorMessageBottom = false;
    }

    $scope.handleMissClick = function($event) {
      $event.stopPropagation();
    }

    $scope.openRemoveRegistrantPopup = function(registrant) {
      $scope.blurForm          = true;
      $scope.modalOpened           = true;
      $scope.registrantToBeRemoved = registrant;
    };

    $scope.closeRemoveRegistrantPopup = function() {
      $scope.blurForm              = false;
      $scope.modalOpened           = false
      $scope.registrantToBeRemoved = null;
    };

    $scope.confirmRemoveRegistrant = function(registrant) {
      $rootScope.showLoader  = true;
      $scope.modalOpened = false;
      removeRegistrant(formId, cartId, registrant).then(function(response) {
        $scope.blurForm            = false;
        $rootScope.showLoader      = false;

        $scope.registrants         = response.registrants;
        $scope.totalPaymentDetails = response.total_payment_details;
        $scope.finishedLoadingData = true;

        var localStorageForms      = JSON.parse(localStorage.active_expoforms);
        for (var i = 0; i < localStorageForms.length; i++) {
          if (localStorageForms[i].formId === formId) {
            localStorageForms[i].cart = $scope.registrants;
          }
        };

        localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

        $timeout(function() {
          var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
          parent.postMessage(heightToSend, '*');
        }, 0);
      }, function(error) {
        if(error.error_type == 'complete'){
          $rootScope.showLoader = true;
          $scope.cartInformationSaved = true;

          $timeout(function() {
            $state.go('registration.confirmation');
          }, 200);
        }
        console.log(error);
      })
    };

    $scope.applyPromoCode = function(promoCode) {
      $scope.blurForm            = true;
      $rootScope.showLoader      = true;
      cartDataService.applyPromoCode(formId, cartId, promoCode).then(function(response) {
        $scope.promoCodeSuccess = true;
        $scope.promoCodeError   = null;

        $scope.registrants         = response.registrants;
        $scope.totalPaymentDetails = response.total_payment_details;
        $scope.finishedLoadingData = true;
        $scope.blurForm            = false;
        $rootScope.showLoader      = false;

        registrantFlowService.facebookTrackingEvent('Expo_PromoCodeUsed', {event: $stateParams.id, Promo_Code: promoCode, value: $scope.totalPaymentDetails.discount});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_PromoCodeEC','eventLabel': $stateParams.id});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_PromoCodeName','eventLabel': promoCode});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_PromoCodeValue','eventLabel': String($scope.totalPaymentDetails.discount)});

        $timeout(function() {
          $scope.promoCodeSuccess = false;
        }, 2000)
      }, function(error) {
        if(error.error_type == 'complete'){
          $rootScope.showLoader = true;
          $scope.cartInformationSaved = true;

          $timeout(function() {
            $state.go('registration.confirmation');
          }, 200);
        }
        $scope.promoCodeError   = error.detail;
        $scope.blurForm            = false;
        $rootScope.showLoader      = false;
      });
    };

    $scope.removePromoCodeFromRegistrant = function(registrantId) {
      $rootScope.showLoader = true;
      $scope.blurForm   = true;
      cartDataService.removePromoCode(formId, cartId, registrantId).then(function(response) {
        $scope.registrants         = response.registrants;
        $scope.totalPaymentDetails = response.total_payment_details;
        $scope.finishedLoadingData = true;

        $rootScope.showLoader = false;
        $scope.blurForm       = false;
      }, function(error) {
        if(error.error_type == 'complete'){
          $rootScope.showLoader = true;
          $scope.cartInformationSaved = true;

          $timeout(function() {
            $state.go('registration.confirmation');
          }, 200);
        }
        console.log(error);
      })
    };

    $scope.checkout                     = function(clickLocation) {
      var isCartValid                   = validateCart($scope.registrants);

      if (isCartValid) {
        $scope.cartInformationSaved       = true;
        $rootScope.showLoader             = true;
        var paymentPage = $rootScope.registration.paymentProvider == 'stripe' ? 'registration.payment' : 'registration.paymentfis';
        registrantFlowService.saveRoute('cart');

        setTimeout(function () {
          $state.go(paymentPage, {
            "totalToPay": $scope.totalPaymentDetails.total
          });
        }, 200);
      } else {
        if (clickLocation === 'top') {
          $scope.errorMessageTop    = "Your cart has errors. Please correct them and then checkout!"
        } else if (clickLocation === 'bottom') {
          $scope.errorMessageBottom = "Your cart has errors. Please correct them and then checkout!"
        }
      }
    }

    $scope.goToAttendeeInfo             = function() {
      $rootScope.showLoader = true;
      $scope.cartInformationSaved = true;

      $timeout(function() {
        $state.go('registration.attendee_info');
      }, 200);
    }

    $scope.navigateBack = function() {
      $scope.previousButtonDisabled = true;
      registrantFlowService.navigateBack();
    }

    function validateCart(registrants) {
      var cartIsValid = true;

      for (var i = 0; i < registrants.length; i++) {
        var currentRegistrant = registrants[i];
        var errorsOnRegistrantObject = currentRegistrant.registrant.errors;
        var errorsOnDiscount         = currentRegistrant.payment_details.discount ? currentRegistrant.payment_details.discount.errors : [];

        if ((errorsOnRegistrantObject && errorsOnRegistrantObject.length > 0) || (errorsOnDiscount && errorsOnDiscount.length > 0)) {
          cartIsValid = false;
          break;
        }
      }
      return cartIsValid;
    }

    function getCart(formId, cartId) {
      cartDataService.getCartItems(formId, cartId).then(function(response) {
        if(!response.completed){
          $scope.registrants         = response.registrants;
          $scope.totalPaymentDetails = response.total_payment_details;
          $scope.finishedLoadingData = true;
          $rootScope.showLoader      = false;

          var localStorageForms      = JSON.parse(localStorage.active_expoforms);
          for (var i = 0; i < localStorageForms.length; i++) {
            if (localStorageForms[i].formId === formId) {
              localStorageForms[i].cart = $scope.registrants;
            }
          };

          for (var i = 0; i < $scope.registrants.length; i++) {
            if (($scope.registrants[i].errors && $scope.registrants[i].errors.length > 0) || ($scope.registrants[i].payment_details.discount && $scope.registrants[i].payment_details.discount.errors && $scope.registrants[i].payment_details.discount.errors.length > 0)) {
              $scope.foundErrors = true;
              break;
            }
          };

          localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

          registrantFlowService.facebookTrackingEvent('Expo_Cart', {event: $stateParams.id, Attendee_Count: $scope.registrants.length, value: $scope.totalPaymentDetails.total});
          registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CartEC','eventLabel': $stateParams.id});
          registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CartNumAttendee','eventLabel': String($scope.registrants.length)});
          registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CartValue','eventLabel': String($scope.totalPaymentDetails.total)});

          $timeout(function() {
            var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
            parent.postMessage(heightToSend, '*');
          }, 0);
        }
        else {
          $timeout(function() {
            $state.go('registration.confirmation');
          }, 200);
        }
      }, function(error) {
        console.log(error);
      });
    }

    function removeRegistrant(formId, cartId, registrant) {
      var deferred = $q.defer();

      cartDataService.removeRegistrantFromCart(formId, cartId, registrant).then(function(response) {
        deferred.resolve(response);
      }, function(error) {
        deferred.reject(error);
      });

      return deferred.promise;
    }

  }
})();
