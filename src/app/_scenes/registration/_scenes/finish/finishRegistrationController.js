(function () {
  'use strict';

  var controllerId = 'finishRegistrationController';
	var module 			 = angular.module('expoform.registration.confirmation');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
    'finishRegistrationDataService',
    '$timeout',
    'registrantFlowService',
		finishRegistrationController
	]
	module.controller(controllerId, dependecies);

  function finishRegistrationController($rootScope, $scope, $state, $stateParams, finishRegistrationDataService, $timeout, registrantFlowService) {
    $scope.finishedLoadingData = false;
    $rootScope.showLoader      = true;
    $scope.paymentType         = "";
    var formId     = $rootScope.registration.formId;
    var cartId     = $rootScope.registration.cartId;
    var receiptUrl = "";

    parent.postMessage({'task': 'scroll_top'}, '*');

    getReceiptUrl(formId, cartId);

    $scope.downloadReceipt = function() {
      registrantFlowService.facebookTrackingEvent('Expo_ReceiptDownload', {event: $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_ReceiptDownloadEC','eventLabel': $stateParams.id});

      var anchor = angular.element('<a/>');
      anchor.css({display: 'none'}); // Make sure it's not visible
      angular.element(document.body).append(anchor); // Attach to document

      anchor.attr({
              href: receiptUrl,
              target: '_blank',
              download: "RECEIPT_" + cartId + moment().format('YYYY-MM-DD / HH:mm:ss')
      })[0].click();

      anchor.remove();
    }

    $scope.finishRegistration = function() {
      $rootScope.showLoader             = true;
      var localStorageForms             = JSON.parse(localStorage.active_expoforms);

      for (var i = 0; i < localStorageForms.length; i++) {
        if (localStorageForms[i].formId === formId) {
          localStorageForms.splice(i, 1);
        }
      };
      localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));
      $scope.registrantInformationSaved = true;

      if ($rootScope.registration.redirectUrlDone != null) {
        parent.postMessage(JSON.stringify({func:'redirect',redirect:$rootScope.registration.redirectUrlDone}), '*');
      }
      parent.postMessage({'task': 'close_lightbox'}, '*');

      $timeout(function() {
        $state.go('registration.attendee_info', {}, {reload: true});
      }, 200);
    }

    function getReceiptUrl(formId, cartId) {
      finishRegistrationDataService.getReceiptUrl(formId, cartId).then(function(response) {
        receiptUrl = response.receipt_url;
        $scope.paymentType = response.payment_type;
        $scope.finishedLoadingData = true;
        $rootScope.showLoader      = false;
        $timeout(function() {
          var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
          parent.postMessage(heightToSend, '*');
        }, 0);
      }, function(error) {
        console.log(error);
      })
    }

  }
})();
