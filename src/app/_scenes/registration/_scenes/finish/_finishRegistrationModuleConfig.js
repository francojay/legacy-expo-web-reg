'use strict'
var finishRegistrationModule	 = angular.module('expoform.registration.finish')
var dependeciesArray					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configFinishRegistration
];
finishRegistrationModule.config(dependeciesArray);

function configFinishRegistration($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.finish', {
		url: '/finish',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/finish/finishRegistrationView.html',
				controller: 'finishRegistrationController'
			}
		}
	})
}
