(function(){
	'use strict'
  var serviceId		 = 'finishRegistrationDataService';
	var module 			 = angular.module('expoform.registration.finish');
	var dependencies = [
		'$q',
		'requestService',
		finishRegistrationDataService
	];

	module.service(serviceId, dependencies);

	function finishRegistrationDataService($q, requestService) {
		var service = {};

		service.getReceiptUrl = function(formId, cartId) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/receipt/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.error(error.data);
			});

			return deferred.promise;
		};

		return service;
	}
})();
