'use strict'
var sessionsModule		 	 = angular.module('expoform.registration.sessions')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configSessions
];
sessionsModule.config(dependeciesArray);

function configSessions($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.sessions', {
		url: '/sessions',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/sessions/sessionsView.html',
				controller: 'sessionsController'
			}
		},
		params: {
      errors: null
    }
	})
}
