(function() {
  'use strict';

  var controllerId = 'sessionsController';
  var module = angular.module('expoform.registration.sessions');
  var dependecies = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    'sessionsDataService',
    'registrantFlowService',
    '$timeout',
    sessionsController
  ];
  module.controller(controllerId, dependecies);

  function sessionsController($rootScope, $scope, $state, $stateParams, sessionsDataService, registrantFlowService, $timeout) {
    var formId              = $rootScope.registration.formId;
    var cartId              = $rootScope.registration.cartId;

    var conferenceStartDate = $rootScope.registration.conferenceStartDate;
    var conferenceEndDate   = $rootScope.registration.conferenceEndDate;
    var conferenceTimezone  = $rootScope.registration.conferenceTimezone;

    var registrant          = registrantFlowService.registrantObject;
    var registrantLevelId   = registrant.level;

    var nextWasClickedAlready = false;

    $scope.levelPrice      = registrantFlowService.currentRegistrantLevelDetails.price;
    $scope.levelName       = registrantFlowService.currentRegistrantLevelDetails.name;
    $scope.sessions        = [];
    $scope.groupedSessions = [];
    $scope.sessionsTotal   = 0;

    $scope.navigationWarning = {
      visible : false,
      text    : 'Oops, there are other days available.',
      icon    : 'fa-calendar-alt',
      buttons : [
        {
          name        : "View Now",
          actionEvent : "OPEN_DAYS_SELECTOR"
        },
        {
          name        : "No Thanks",
          actionEvent : "CLOSE_POPUPS_ACKNOWLEDGE"
        }
      ]
    }

    sessionsDataService.getAllSessions(formId, registrant, cartId).then(function(response) {
      if (response.data.length === 0) {
        $state.go('registration.guests');
      } else {
        $scope.sessions = response.data;
        $scope.groupedSessions                    = groupSessionsByDay($scope.sessions, conferenceStartDate, conferenceEndDate, conferenceTimezone);
        $scope.groupedSessions                    = matchPreviousSelectedSessions($scope.groupedSessions, registrantFlowService.registrantObject.addon_sessions);
        $scope.currentSessionGroup                = initSelectedGroup($scope.groupedSessions);
        $scope.validGroupsExistBeforeCurrentGroup = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
        $scope.validGroupsExistAfterCurrentGroup  = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
        $scope.finishedLoadingData                = true;
        $rootScope.showLoader                     = false;
        registrantFlowService.saveRoute('sessions');
        $timeout(function() {
          var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
          parent.postMessage(heightToSend, '*');
        }, 0);
      }
    }, function(error) {
      console.log(error);
    });

    parent.postMessage({'task': 'scroll_top'}, '*');

    if ($stateParams.errors != null) {
      $scope.errorMessageTop = $stateParams.errors;
    }

    registrantFlowService.facebookTrackingEvent('Expo_AddOnSession', {event: $stateParams.id});
    registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_AddOnSessionEC','eventLabel': $stateParams.id});

    $scope.selectGroup = function($event, group, index) {
      $event.stopPropagation();
      $scope.currentSessionGroup = {
        group: group,
        index: index
      };

      $scope.dropdownSelectorActive             = false;
      $scope.validGroupsExistBeforeCurrentGroup = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
      $scope.validGroupsExistAfterCurrentGroup  = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
      $scope.groupedSessions[index].wasActive   = true;
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null
      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.toggleDropdownSelector = function($event) {
      $event.stopPropagation();
      $event.preventDefault();
      $scope.dropdownSelectorActive = !$scope.dropdownSelectorActive;
    }

    $scope.selectPreviousGroup = function(currentGroup) {
      var momentStartDate = moment(conferenceStartDate).tz(conferenceTimezone);
      var current = angular.copy(currentGroup);
      for (var day = current.group.day; day > momentStartDate; day.subtract(1, 'd')) {
        current.index--;
        var group = $scope.groupedSessions[current.index];
        if (group.hasOwnProperty('sessionsByHour') && Object.keys(group.sessionsByHour).length > 0) {
          $scope.currentSessionGroup = {
            group: group,
            index: current.index
          }
          $scope.validGroupsExistBeforeCurrentGroup       = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.validGroupsExistAfterCurrentGroup        = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.groupedSessions[current.index].wasActive = true;
          $scope.errorMessageTop    = null;
          $scope.errorMessageBottom = null
          break;
        }
      }

      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.selectNextGroup = function(currentGroup) {
      var momentEndDate = moment(conferenceEndDate).tz(conferenceTimezone);
      var current = angular.copy(currentGroup);
      for (var day = current.group.day; day < momentEndDate; day.add(1, 'd')) {
        current.index++;
        var group = $scope.groupedSessions[current.index];
        if (group.hasOwnProperty('sessionsByHour') && Object.keys(group.sessionsByHour).length > 0) {
          $scope.currentSessionGroup = {
            group: group,
            index: current.index
          }
          $scope.validGroupsExistBeforeCurrentGroup       = checkIfValidGroupsExistBeforeCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.validGroupsExistAfterCurrentGroup        = checkIfValidGroupsExistAfterCurrentGroup($scope.currentSessionGroup, $scope.groupedSessions);
          $scope.groupedSessions[current.index].wasActive = true;
          $scope.errorMessageTop    = null;
          $scope.errorMessageBottom = null
          break;
        }
      }

      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.selectSession = function($event, session) {
      $scope.dropdownSelectorActive = false;

      if ($event) {
        $event.stopPropagation();
      }
      session.selected = !session.selected;

      if (session.selected) {
        $scope.sessionsTotal += session.price;
        registrantFlowService.facebookTrackingEvent('Expo_AddOnSelect', {event: $stateParams.id, session: session.title, value: session.price});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_AddOnSessionEC','eventLabel': $stateParams.id});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_AddOnSessionSessionName','eventLabel': session.title});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_AddOnSessionAddOnPrice','eventLabel': String(session.price)});
      } else {
        $scope.sessionsTotal -= session.price;
      }

      $scope.openedSession = null;
      $rootScope.blurForm = false;
    }

    $scope.openSessionDetails = function($event, session) {
      $event.stopPropagation();
      $scope.dropdownSelectorActive = false;
      $rootScope.blurForm = true;
      $scope.openedSession = session;
      if (session.loaded) {
        $scope.openedSession = session;
      } else {
        sessionsDataService.getOneSession(formId, registrantLevelId, session.session_id, cartId).then(function(response) {
          session.description = response.data.description;
          session.location = response.data.location;
          session.continuing_educations = response.data.continuing_educations;
          session.ce_hours = response.data.ce_hours;
          session.max_capacity = response.data.max_capacity;
          session.session_type = response.data.session_type;
          session.speakers = response.data.speakers;

          session.loaded = true;
          $scope.openedSession = session;

        }, function(error) {
          console.log(error);
        })
      }
    }

    $scope.closeSessionDetails = function($event) {
      $event.stopPropagation();
      $scope.openedSession = null;
      $rootScope.blurForm = false;
    }

    $scope.saveRegistrantAddonSessions = function($event, sessions, location) {
      $event.stopPropagation();
      $event.preventDefault();
      var allSessionDaysHaveBeenSeen = true;
      for (var i = 0; i < sessions.length; i++) {
        allSessionDaysHaveBeenSeen = allSessionDaysHaveBeenSeen && sessions[i].wasActive;
      }

      if (allSessionDaysHaveBeenSeen || nextWasClickedAlready) {
        registrantFlowService.setRegistrantAddonSessions(sessions).then(function(response) {
          $scope.registrantInformationSaved = true;
          $rootScope.showLoader = true;
          setTimeout(function() {
            $state.go('registration.guests');
          }, 200);
        }, function(error) {

        });
        registrantFlowService.savePartialCart(formId, cartId);
      } else {
        if (location === 'bottom') {
          $scope.errorMessageBottom     = 'Oops, there are more days to view! Choose a new day from the list above to view more sessions.';
          $scope.dropdownSelectorActive = true;
        } else if (location === 'top') {
          $scope.errorMessageTop = 'Oops, there are more days to view! Choose a new day from the list to view more sessions.';
          $scope.dropdownSelectorActive = true;
        }
      }
      nextWasClickedAlready = true;
    }

    $scope.navigateBack = function() {
      $scope.previousButtonDisabled = true;
      registrantFlowService.navigateBack();
    }

    function groupSessionsByDay(sessions, conferenceStart, conferenceEnd, timezone) {
      var momentStartDate = moment(conferenceStart).tz(timezone);
      var momentEndDate = moment(conferenceEnd).tz(timezone);
      var groupedSessions = [];
      for (var day = momentStartDate; day < momentEndDate; day.add(1, 'd')) {
        var dayCopy = angular.copy(day);
        var dayObject = {
          dayToDisplay  : day.format('dddd M/D'),
          day           : dayCopy,
          sessions      : [],
          wasActive     : false
        };

        for (var i = 0; i < sessions.length; i++) {
          var currentSession = sessions[i];
          var momentSessionStart = moment(currentSession.starts_at).tz(timezone);
          var momentSessionEnd = moment(currentSession.ends_at).tz(timezone);
          if (momentSessionStart.format('YYYY/M/D') === momentStartDate.format('YYYY/M/D')) {
            dayObject.sessions.push(currentSession);
          }
        }

        if (dayObject.sessions.length > 0) {
          dayObject.sessionsByHour = {};
          for (var i = 0; i < dayObject.sessions.length; i++) {
            var currentSession = dayObject.sessions[i];
            var momentSessionStart = moment(currentSession.starts_at).tz(timezone);
            var momentSessionEnd = moment(currentSession.ends_at).tz(timezone);

            var groupStartKey = momentSessionStart.format('h:00 A');
            var startHour = momentSessionStart.hour();

            if (!dayObject.sessionsByHour[startHour]) {
              dayObject.sessionsByHour[startHour] = {
                keyToShowInView: groupStartKey,
                sessions: []
              };
            }

            currentSession.formattedSessionStart = momentSessionStart.format('h:mm A');
            currentSession.formattedSessionEnd = momentSessionEnd.format('h:mm A');
            currentSession.formattedDay = momentSessionEnd.format('dddd M/D');
            dayObject.sessionsByHour[startHour].sessions.push(currentSession);
          }
        } else {
          dayObject.wasActive = true;
        }

        groupedSessions.push(dayObject);
      };
      return groupedSessions;
    }

    function matchPreviousSelectedSessions(groupedSessions, previouslySelectedSessions) {
      for (var i = 0; i < $scope.groupedSessions.length; i++) {
        var group = $scope.groupedSessions[i];
        for (var j = 0; j < group.sessions.length; j++) {
          for (var k = 0; k < previouslySelectedSessions.length; k++) {
            if (previouslySelectedSessions[k] === group.sessions[j].session_id) {
              $scope.selectSession(null, group.sessions[j]);
            }
          }
        }
      }

      return groupedSessions;
    }

    function initSelectedGroup(groupedSessions) {
      var currentMomentDay = moment();
      var currentGroup = null;
      var currentIndex = null;
      for (var i = 0; i < groupedSessions.length; i++) {
        if (groupedSessions[i].dayToDisplay === currentMomentDay.format('dddd M/D')) {
          currentGroup = groupedSessions[i];
          currentIndex = i;

          groupedSessions[i].wasActive = true;
        }
      }
      if (currentGroup && currentGroup.sessions.length > 0) {
        return {
          group: currentGroup,
          index: currentIndex
        }
      } else {
        for (var i = 0; i < groupedSessions.length; i++) {
          if (groupedSessions[i].sessions.length > 0) {
            currentGroup = groupedSessions[i];
            currentIndex = i;

            groupedSessions[i].wasActive = true;

            break
          }
        }
        return {
          group: currentGroup,
          index: currentIndex
        }
      }
    }

    function checkIfValidGroupsExistBeforeCurrentGroup(currentGroup, groups) {
      var currentGroupIndex = currentGroup.index;
      var currentGroup = currentGroup.group;
      var foundValidGroup = false;
      for (var i = currentGroupIndex - 1; i >= 0; i--) {
        if (groups[i].sessions.length > 0) {
          foundValidGroup = true;
          break
        }
      }

      return foundValidGroup;
    }

    function checkIfValidGroupsExistAfterCurrentGroup(currentGroup, groups) {
      var currentGroupIndex = currentGroup.index;
      var currentGroup = currentGroup.group;
      var foundValidGroup = false;
      for (var i = currentGroupIndex + 1; i < groups.length; i++) {
        if (groups[i].sessions.length > 0) {
          foundValidGroup = true;
          break
        }
      }

      return foundValidGroup;
    }

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation()
      $scope.errorMessageTop        = null;
      $scope.errorMessageBottom     = null;
    }

    $rootScope.$on("CLOSE_DROPDOWN", function(event, data) {
      $scope.dropdownSelectorActive = false;
    });

  }
})();
