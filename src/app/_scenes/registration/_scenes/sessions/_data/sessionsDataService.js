(function(){
	'use strict'
  var serviceId		 = 'sessionsDataService';
	var module 			 = angular.module('expoform.registration.sessions');
	var dependencies = [
		'$q',
		'requestService',
		sessionsDataService
	];

	module.service(serviceId, dependencies);

	function sessionsDataService($q, requestService) {
		var service = {};

		service.getAllSessions = function(formId, registrant, cartId) {
			var deferred = $q.defer()
			var url 		 = "forms/" + formId + "/registration_levels/" + registrant.level + "/addon_sessions/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.getOneSession = function(formId, levelId, sessionId, cartId) {
			var deferred = $q.defer();

			var url			 = "forms/" + formId + "/registration_levels/" + levelId + "/addon_sessions/" + sessionId + "/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			})

			return deferred.promise;
		}

		return service;
	}
})();
