(function(){
	'use strict'
  var serviceId		 = 'confirmationDataService';
	var module 			 = angular.module('expoform.registration.confirmation');
	var dependencies = [
		'$q',
		'requestService',
		confirmationDataService
	];

	module.service(serviceId, dependencies);

	function confirmationDataService($q, requestService) {
		var service = {};

		service.getRegistrantsEmailAddresses = function(formId, cartId) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/cart/mails/";

			requestService.get(url, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.error(error.data);
			});

			return deferred.promise;
		};

		service.sendEmailsToRegistrants = function(formId, cartId, emailList) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/cart/mails/";

			var data 		 = {
				"email_addresses": emailList
			};

			requestService.post(url, data, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		}

		return service;
	}
})();
