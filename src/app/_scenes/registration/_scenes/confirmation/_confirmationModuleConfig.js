'use strict'
var confirmationModule	 = angular.module('expoform.registration.confirmation')
var dependeciesArray		 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configConfirmation
];
confirmationModule.config(dependeciesArray);

function configConfirmation($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.confirmation', {
		url: '/confirmation',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/confirmation/confirmationView.html',
				controller: 'confirmationController'
			}
		},
		params: {
      totalToPay: null
    }
	})
}
