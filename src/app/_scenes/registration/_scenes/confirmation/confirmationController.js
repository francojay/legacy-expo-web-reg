(function () {
  'use strict';

  var controllerId = 'confirmationController';
	var module 			 = angular.module('expoform.registration.confirmation');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'confirmationDataService',
    '$timeout',
    'registrantFlowService',
		confirmationController
	]
	module.controller(controllerId, dependecies);

  function confirmationController($rootScope, $scope, $state, $stateParams, confirmationDataService, $timeout, registrantFlowService) {
    var formId            = $rootScope.registration.formId;
    var cartId            = $rootScope.registration.cartId;
    $rootScope.showLoader = true;
    getEmails(formId, cartId);
    parent.postMessage({'task': 'scroll_top'}, '*');
    registrantFlowService.facebookTrackingEvent('Expo_RegistrationComplete', {event: $stateParams.id, value: $state.params.totalToPay});
    registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_RegistrationCompleteEC','eventLabel': $stateParams.id});
    registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_RegistrationCompletePrice','eventLabel': String($state.params.totalToPay)});

    $scope.registrantEmails = [

    ];

    $scope.selectAttendee = function(attendee) {
      attendee.selected = attendee.selected ? false : true;
    }

    $scope.selectAll = function(bool) {
      for(var i = 0; i< $scope.registrantEmails.length; i++){
        $scope.registrantEmails[i].selected = bool;
      }
    }

    $scope.sendEmails = function(registrantEmails) {
      $rootScope.showLoader              = true;
      $scope.sendingEmails               = true;
      var emailAddressesToReceiveTickets = [];
      for (var i = 0; i < registrantEmails.length; i++) {
        if (registrantEmails[i].selected) {
          emailAddressesToReceiveTickets.push(registrantEmails[i].email)
        }
      }

      registrantFlowService.facebookTrackingEvent('Expo_ConfirmationSent', {event: $stateParams.id, available: registrantEmails.length, sent: emailAddressesToReceiveTickets.length});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_ConfirmationSentEC','eventLabel': $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_ConfirmationAvail','eventLabel': String(registrantEmails.length)});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_ConfirmationSent','eventLabel': String(emailAddressesToReceiveTickets.length)});

      confirmationDataService.sendEmailsToRegistrants(formId, cartId, emailAddressesToReceiveTickets).then(function(response) {
        $scope.emailsAreSent              = true;
        $rootScope.showLoader             = true;

        var localStorageForms      = JSON.parse(localStorage.active_expoforms);
        for (var i = 0; i < localStorageForms.length; i++) {
          if (localStorageForms[i].formId === formId) {
            localStorageForms[i].confirmationSent = true;
          }
        };
        localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

        setTimeout(function () {
          $state.go('registration.finish');
        }, 200);
      }, function(error) {

      })


    }

    function getEmails(formId, cartId) {
      confirmationDataService.getRegistrantsEmailAddresses(formId, cartId).then(function(response) {
        for (var i = 0; i < response.email_addresses.length; i++) {
          var registrantEmailObject = {
            "email"    : response.email_addresses[i],
            "selected" : true
          }
          $scope.registrantEmails.push(registrantEmailObject);
        }
        $rootScope.showLoader = false;
        $scope.finishedLoadingData = true;
        $timeout(function() {
          var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
          parent.postMessage(heightToSend, '*');
        }, 0);
      }, function(error) {
        console.log(error);
      })
    }

  }
})();
