'use strict'
var paymentModule		 		 = angular.module('expoform.registration.payment')
var dependeciesArray 					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configPayment
];
paymentModule.config(dependeciesArray);

function configPayment($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.payment', {
		url: '/payment',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//console.log($stateParams.id)
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/payment/paymentView.html',
				controller: 'paymentController'
			}
		},
		params: {
      totalToPay: null
    }
	})
}
