(function() {
  'use strict';

  var controllerId = 'paymentController';

  var module      = angular.module('expoform.registration.payment');
  var dependecies = [
    '$rootScope',
    '$scope',
    '$state',
    '$stateParams',
    'paymentDataService',
    'stripeService',
    'registrantFlowService',
    '$timeout',
    paymentController
  ]
  module.controller(controllerId, dependecies);

  function paymentController($rootScope, $scope, $state, $stateParams, paymentDataService, stripeService, registrantFlowService, $timeout) {
    var formId          = $rootScope.registration.formId;
    var cartId          = $rootScope.registration.cartId;

    $scope.routesLength = registrantFlowService.getRoutesLength();
    if ($state.params.totalToPay <= 0) {
      $scope.paymentInProgress = true;
      $rootScope.showLoader    = true;
      completePayment();
    } else {
      if ($scope.routesLength > 0) {
        registrantFlowService.saveRoute('payment');
        $scope.routesLength++;
      }

      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }
    parent.postMessage({'task': 'scroll_top'}, '*');

    $scope.totalToPay = $state.params.totalToPay;
    $scope.canPay = false;
    $scope.acceptedTerms  = false;
    $scope.cardError = null;
    $scope.payments = [];

    for (var i = 0; i < $rootScope.registration.paymentTypes.length; i++) {
      if($rootScope.registration.paymentTypes[i].active) {
        $scope.payments[i] = $rootScope.registration.paymentTypes[i];
        $scope.payments[i].label = $scope.payments[i].name.replace("_"," ");
        $scope.payments[i].selected = false;
      }

    }
    $scope.selectedPayment = $scope.payments[0].name;
    $scope.payments[0].selected = true;


    $scope.additionalPaymentFields = [
      {
        name  : "Billing Zip Code",
        code  : "address_zip",
        visibility  : "required",
        value : ""
      },
      {
        name  : "Name on Card",
        code  : "name",
        visibility  : "required",
        value : ""
      },
      {
        name  : "Billing Email",
        code  : "address_line2",
        visibility  : "required",
        value : ""
      }
    ];

    $scope.checkPaymentFields = [
      {
        name  : "Billing Email",
        code  : "address_line2",
        visibility  : "required",
        value : ""
      }
    ];


    var stripe = stripeService.initializeStripe();
    var cardNumber = stripeService.cardNumber;
    if ($state.params.totalToPay > 0) {
      $scope.finishedLoadingData = true;
      $rootScope.showLoader = false;

      registrantFlowService.facebookTrackingEvent('Expo_CCard', {event: $stateParams.id, value: $scope.totalToPay});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CCard','eventLabel': $stateParams.id});
      registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_CCardPrice','eventLabel': String($scope.totalToPay)});
    }

    $scope.checkIfNeedsValidation   = function(field) {
      field.error = false;
      if (field && field.code === 'address_line2') {
        field = validateEmailField(field);
      }
    }

    $scope.payCart = function(clickLocation) {
      if (!$scope.paymentInProgress) {
      var paymentCanBeCompleted = checkIfCanEnablePayButton($scope.selectedPayment);
        if (paymentCanBeCompleted && $scope.acceptedTerms && ($scope.selectedPayment == null || $scope.selectedPayment == 'credit_card')) {
          $scope.cardError = null;
          if ($scope.canPay) {
            var options = {};
            for (var i = 0; i < $scope.additionalPaymentFields.length; i++) {
              options[$scope.additionalPaymentFields[i].code] = $scope.additionalPaymentFields[i].value;
            }
            $scope.paymentInProgress     = true;
            $rootScope.showLoader        = true;
            stripe.createToken(cardNumber, options).then(completePayment);
          }
        } else if (paymentCanBeCompleted && $scope.acceptedTerms && $scope.selectedPayment == 'check') {
          $scope.paymentInProgress     = true;
          $rootScope.showLoader        = true;
           completeCheckPayment();
        }
        else  {
          if (!paymentCanBeCompleted) {
            if (clickLocation === 'bottom') {
              $scope.errorMessageBottom = 'Please complete all the required fields.';
            } else if (clickLocation === 'top') {
              $scope.errorMessageTop = 'Please complete all the required fields.';
            }
          } else if (!$scope.acceptedTerms) {
            if (clickLocation === 'top') {
              $scope.errorMessageTop = "Please accept the Terms & Conditions!";
            } else if (clickLocation === 'bottom') {
              $scope.errorMessageBottom = "Please accept the Terms & Conditions";
            }
          }
        }
      }

    }

    $scope.navigateBack = function() {
      $scope.previousButtonDisabled = true;
      registrantFlowService.navigateBack();
    }

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation();
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null;
    }

    function checkIfCanEnablePayButton(selectedPayment) {
      var canPay = true;
      if(selectedPayment == null || selectedPayment == 'credit_card') {
        for (var i = 0; i < $scope.additionalPaymentFields.length; i++) {
          var field      = $scope.additionalPaymentFields[i];
          var visibility = field.visibility;
          var value      = field.value;
          var error      = field.error;

          if(field.code === 'address_line2') {
              field = validateEmailField(field);
              error = field.error;
          }
          if ((visibility === 'required' && (value === "" || !value)) || error) {
            canPay      = false;
            field.error = true;
          }  else {
            field.error = false;
          }
        }
        for (var i = 0; i < stripeService.savedErrors.length; i++) {
          if (stripeService.savedErrors[i] != null) {
            canPay = false;
            break;
          }
        }
      }
      else {
        for (var i = 0; i < $scope.checkPaymentFields.length; i++) {
          var field      = $scope.checkPaymentFields[i];
          var visibility = field.visibility;
          var value      = field.value;
          var error      = field.error;

          if(field.code === 'address_line2') {
              field = validateEmailField(field);
              error = field.error;
          }
          if ((visibility === 'required' && (value === "" || !value)) || error) {
            canPay      = false;
            field.error = true;
          }  else {
            field.error = false;
          }
        }
      }

      $scope.canPay = canPay;
      return canPay;
    };

    function completePayment(result) {
      $rootScope.loaderUserMessage = "We are processing your registrants, please wait...";
      var token = (result && !result.error) ? result.token.id : (!result? null: "undefined");
      if (token != "undefined") {
        var paymentObject = {
           "payment_method": $scope.selectedPayment,
	         "stripe_token": token
        };
        paymentDataService.completePayment(formId, cartId, paymentObject).then(function(response) {
          var localStorageForms      = JSON.parse(localStorage.active_expoforms);
          for (var i = 0; i < localStorageForms.length; i++) {
            if (localStorageForms[i].formId === formId) {
              localStorageForms[i].paymentDone = true;
            }
          };
          localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

          setTimeout(function() {
            $rootScope.loaderUserMessage = null;
            $state.go('registration.confirmation',{totalToPay : $scope.totalToPay});
          }, 200);
        }, function(error) {
          if(error.error_type == 'cart'){
            $state.go('registration.cart', { errors:error.detail });
          }
          $scope.cardError = error.detail;
          $scope.paymentDone = false;
          $scope.paymentInProgress = false;
          $rootScope.showLoader    = false;
          $rootScope.loaderUserMessage = null;
        });
      } else {
        stripeService.checkErrorsStripeElements(result);
        $scope.paymentDone = false;
        $scope.finishedLoadingData = true;
        $scope.paymentInProgress = false;
        $rootScope.showLoader    = false;
        $rootScope.loaderUserMessage = null;
        $scope.$apply();
      }
    }

    function completeCheckPayment() {
      $rootScope.loaderUserMessage = "We are processing your registrants, please wait...";
      var paymentObject = {
         "payment_method": $scope.selectedPayment,
         "billing_email_address": $scope.checkPaymentFields[0].value
      };
      paymentDataService.completePayment(formId, cartId, paymentObject).then(function(response) {
        var localStorageForms      = JSON.parse(localStorage.active_expoforms);
        for (var i = 0; i < localStorageForms.length; i++) {
          if (localStorageForms[i].formId === formId) {
            localStorageForms[i].paymentDone = true;
          }
        };
        localStorage.setItem('active_expoforms', JSON.stringify(localStorageForms));

        setTimeout(function() {
          $rootScope.loaderUserMessage = null;
          $state.go('registration.confirmation',{totalToPay : $scope.totalToPay});
        }, 200);
      }, function(error) {
        if(error.error_type == 'cart'){
          $state.go('registration.cart', { errors:error.detail });
        }
        $scope.cardError = error.detail;
        $scope.paymentDone = false;
        $scope.paymentInProgress = false;
        $rootScope.showLoader    = false;
        $rootScope.loaderUserMessage = null;
      });
    }

    function validateEmailField(field) {
      var address = field.value;
      if (address.length > 0) {
        var regexExpression = /[a-zA-Z0-9+_-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
        var match =   address.match(regexExpression);
        field.error = !regexExpression.test(address) || match.toString() != address;
      }
      return field;
    }

    $scope.openModal = function($event, url) {
      $event.stopPropagation();
      $rootScope.blurForm           = true;
      $scope.openedModal          = true;
      registrantFlowService.getFileContent(url).then(function(response) {
        $scope.fileContent = response.data.text;
      }, function(error) {
        console.log('error');
      });

    }

    $scope.closeModal = function($event) {
      $event.stopPropagation();
      $scope.openedModal = null;
      $rootScope.blurForm = false;
    }

    $scope.acceptTerms = function() {
        $scope.acceptedTerms = !$scope.acceptedTerms;
    }

    $scope.changePaymentType = function(payment){
      for (var i = 0; i < $scope.payments.length; i++) {
          $scope.payments[i].selected = false;
      }
      payment.selected = true;
      $scope.selectedPayment = payment.name;
      $scope.acceptedTerms = false;
    }
  }
})();
