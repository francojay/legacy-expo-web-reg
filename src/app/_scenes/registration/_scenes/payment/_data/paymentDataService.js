(function(){
	'use strict'
  var serviceId		 = 'paymentDataService';
	var module 			 = angular.module('expoform.registration.payment');
	var dependencies = [
		'$q',
		'requestService',
		paymentDataService
	];

	module.service(serviceId, dependencies);

	function paymentDataService($q, requestService) {
		var service = {};

		service.completePayment = function(formId, cartId, paymentObject) {
			var deferred = $q.defer();
			var url 		 = "forms/" + formId + "/complete/";

			requestService.post(url, paymentObject, cartId).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error.data);
			});

			return deferred.promise;
		};

		return service;
	}
})();
