'use strict'
var attendeeInfoModule		 		 = angular.module('expoform.registration.attendee_info')
var dependeciesArray 					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configAttendeeInfo
];
attendeeInfoModule.config(dependeciesArray);

function configAttendeeInfo($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.attendee_info', {
		url: '/info',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/attendee_info/attendeeInfoView.html',
				controller: 'attendeeInfoController'
			}
		},
		params: {
      errors: null
    }
	})
}
