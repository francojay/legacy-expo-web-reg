
(function () {
  'use strict';

  var controllerId = 'attendeeInfoController';
	var module 			 = angular.module('expoform.registration.attendee_info');
	var dependencies = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'attendeeInfoDataService',
		'requestService',
    '_meta',
    'registrantFlowService',
    '$timeout',
    'API',
    '$sce',
    '$filter',
		attendeeInfoController
	]
	module.controller(controllerId, dependencies);

  function attendeeInfoController($rootScope, $scope, $state, $stateParams, attendeeInfoDataService, requestService, _meta, registrantFlowService, $timeout, API, $sce, $filter) {
    registrantFlowService.saveRoute('attendee_info');

    var formId         = $rootScope.registration.formId;
    var cartId         = $rootScope.registration.cartId;
    var placesInstance = null;
    var componentForm  = {
        street_number               : 'short_name',
        route                       : 'long_name',
        locality                    : 'long_name',
        administrative_area_level_1 : 'short_name',
        country                     : 'long_name',
        postal_code                 : 'short_name'
      };



    $scope.shouldShowThePreviousButton = ($rootScope.registration.firstStep === 'attendee_first') ? false : true;
    $scope.consent                     = $sce.trustAsHtml($rootScope.registration.consent);
    $scope.acceptedTerms               = false;
    $scope.attendeeFields              = [];
    $scope.countryIndex                = null;
    $scope.stateIndex                  = null;

    getAttendeeFields(formId, cartId);

    $scope.countries                   = API.COUNTRIES;
    $scope.unitedStates                = API.UNITED_STATES;

    if($stateParams.errors != null){
      $scope.errorMessageTop = $stateParams.errors;
    }
    registrantFlowService.facebookTrackingEvent('Expo_ProfileLoad', {event: $stateParams.id});
    registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_ProfileEC','eventLabel': $stateParams.id});

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation()
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null;
    }

    $scope.checkIfNeedsValidation = function(event,field) {
      event.stopPropagation();
      if (field.code === 'email_address') {
        field = validateEmailField(field);
      }
      $scope.checkIfCanEnableNextButton();
    }

    $scope.toggleSelectionForCustomFieldOption = function($event, field, option) {
      $event.stopPropagation();
      if (field.customFieldType === 2) {
        for (var i = 0; i < field.options.length; i++) {
          field.options[i].selected = false;
        }
        option.selected = true;
        field.value = option.value;
        field.openOptions = false;

        document.getElementsByClassName('_body')[0].setAttribute('style','height:auto;');
        $timeout(function() {
         var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
         parent.postMessage(heightToSend, '*');

        }, 0);
      } else {
        field.value = "";
        for (var i = 0; i < field.options.length; i++) {
          if (field.options[i].option_id === option.option_id) {
            field.options[i].selected = !field.options[i].selected;
          }
          if (field.options[i].selected) {
            if (field.value.length === 0) {
              field.value = field.options[i].value
            } else {
              field.value += ", " + field.options[i].value;
            }
          }
        }
      }

    }

    $scope.openOptionsPanel = function($event, field) {
      $event.stopPropagation();
      $event.preventDefault();
      for (var i = 0; i < $scope.attendeeFields.length; i++) {
        $scope.attendeeFields[i].openOptions = false;
      };

      field.openOptions = !field.openOptions;
      var bodyRect = document.getElementsByClassName('_body')[0].getBoundingClientRect().top,
        heightToSend = document.getElementsByClassName('_footer')[0].getBoundingClientRect().top;
        heightToSend   = heightToSend - bodyRect;

      var dropdown     =  $event.target.getBoundingClientRect().top + 45 + 166 + 7 - bodyRect;
      //console.log(dropdown);

      var heightToSend = document.getElementsByClassName('_footer')[0].getBoundingClientRect().top;
      var dropdown     = $event.target.getBoundingClientRect().top + 45 + 166;
      if (dropdown > heightToSend) {
        document.getElementsByClassName('_body')[0].setAttribute('style','height:'+dropdown+'px');
      }

      $timeout(function() {
        var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
        parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.checkIfCanEnableNextButton = function() {
      var canGo = true;
      for (var i = 0; i < $scope.attendeeFields.length; i++) {
        var field = $scope.attendeeFields[i];

        var visibility = field.visibility;
        var value      = field.value;
        var error      = field.error;

        if (error) {
          canGo = false;
          break
        } else if (visibility === 'required' && (value === "" || !value)) {
          canGo = false;
          break;
        }
      }
      $scope.canGoToNextStep = canGo && $scope.acceptedTerms;
    }

    $scope.saveRegistrantInformation = function(attendeeFields, clickLocation) {
      var informationIsValid = validateAttendeeInformation(attendeeFields);

      if (informationIsValid && $scope.acceptedTerms) {
        var registrant = attendeeFields;

        registrantFlowService.setRegistrantInformation(registrant).then(function(success) {
          $scope.registrantInformationSaved = true;
          $rootScope.showLoader             = true;
          setTimeout(function () {
            $state.go('registration.levels');
          }, 200);

        });
        registrantFlowService.savePartialCart(formId, cartId);
    } else {
      if (!informationIsValid) {
        if (clickLocation === 'bottom') {
          $scope.errorMessageBottom = "Please complete all the required fields.";
        } else if (clickLocation === 'top') {
          $scope.errorMessageTop = "Please complete all the required fields.";
        }
      } else if (!$scope.acceptedTerms) {
        if (clickLocation === 'bottom') {
          $scope.errorMessageBottom = "Please accept the Terms and Conditions!";
        } else if (clickLocation) {
          $scope.errorMessageTop = "Please accept the Terms and Conditions!";

        }
      }
    }

    }

    $scope.selectState = function($event, field, state) {
      $event.stopPropagation();
      for (var i = 0; i < $scope.unitedStates.length; i++) {
        $scope.unitedStates[i].selected = false;
      }
      state.selected = true;
      field.value    = state.abbreviation;
      field.openOptions = false;
      document.getElementsByClassName('_body')[0].setAttribute('style','height:auto;');
      $timeout(function() {
       var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
       parent.postMessage(heightToSend, '*');
      }, 0);
    }

    $scope.selectCountry = function($event, field, country) {
      $event.stopPropagation();

      for (var i = 0; i < $scope.countries.length; i++) {
        $scope.countries[i].selected = false;
      }

      country.selected = true;
      field.value      = country.code;
      field.openOptions = false;
      document.getElementsByClassName('_body')[0].setAttribute('style','height:auto;');
      $timeout(function() {
       var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
       parent.postMessage(heightToSend, '*');
      }, 0);
    }

    function validateAttendeeInformation(attendeeFields, customFields) {
      var basicAttendeeInformationIsValid  = true;
      var customAttendeeInformationIsValid = true;

      for (var i = 0; i < attendeeFields.length; i++) {
        if (attendeeFields[i].visibility === 'required' && (!attendeeFields[i].value || (attendeeFields[i].value && (attendeeFields[i].value.length < 2 || attendeeFields[i].value.length > 255)))) {
          attendeeFields[i].error = true;
          basicAttendeeInformationIsValid = false;
          if (attendeeFields[i].code === 'phone_number') {
            var phoneNumberRegex     = /^[\+]?[\s]?[0-9]{0,2}[-\s\.]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{2,3}[-\s\.]?[0-9]{3,6}$/;
            var isPhoneNumberCorrect = phoneNumberRegex.test(attendeeFields[i].value);
            attendeeFields[i].error  = !isPhoneNumberCorrect;
            if (attendeeFields[i].error) {
              basicAttendeeInformationIsValid = false;
            }
          }
        } else if (attendeeFields[i].code === "email_address" && attendeeFields[i].value) {
          attendeeFields[i] = validateEmailField(attendeeFields[i]);
          if (attendeeFields[i].error) {
            basicAttendeeInformationIsValid = false;
          }
        } else if (attendeeFields[i].code === 'phone_number' && attendeeFields[i].value) {
          var phoneNumberRegex     = /^[\+]?[\s]?[0-9]{0,2}[-\s\.]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{2,3}[-\s\.]?[0-9]{3,6}$/;
          var isPhoneNumberCorrect = phoneNumberRegex.test(attendeeFields[i].value);
          attendeeFields[i].error  = !isPhoneNumberCorrect;
          if (attendeeFields[i].error) {
            basicAttendeeInformationIsValid = false;
          }
        } else {
          attendeeFields[i].error  = false;
        }
      }

      // for (var i = 0; i < customFields.length; i++) {
      //   if (customFields[i].visibility === 'required' && !customFields[i].value) {
      //     customFields[i].error = true;
      //     customAttendeeInformationIsValid    = false;
      //   } else {
      //     customFields[i].error = false;
      //     //customAttendeeInformationIsValid    = true;
      //   }
      // }

      return basicAttendeeInformationIsValid; //&& customAttendeeInformationIsValid;
    }

    function validateEmailField(field) {
      var address = field.value;
      if (address && address.length > 0) {
        var regexExpression = /[a-zA-Z0-9+_-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/;
        var match =   address.match(regexExpression);
        field.error = !regexExpression.test(address) || match.toString() != address ;
      }
      return field;
    }

    function getAttendeeFields(formId, cartId) {
      attendeeInfoDataService.getAttendeeFields(formId, cartId).then(function(response) {

        for (var i = 0; i < response.standard_fields.length; i++) {
          var attendeeFieldObject = {
                name  : response.standard_fields[i].name.replace("_", " "),
                code  : response.standard_fields[i].name,
                visibility  : response.standard_fields[i].visibility,
                value : registrantFlowService.registrantObject.registrant[response.standard_fields[i].name] || "",
                order : response.standard_fields[i].order
          };
          $scope.attendeeFields.push(attendeeFieldObject);
        }
        for (var i = 0; i < response.custom_fields.length; i++) {
          if(response.custom_fields[i].visibility != 'hidden'){
            var attendeeFieldObject = {
              name             : response.custom_fields[i].name,
              code             : response.custom_fields[i].id,
              visibility       : response.custom_fields[i].visibility,
              value            : "",
              options          : response.custom_fields[i].options,
              customFieldType  : response.custom_fields[i].type,
              order            : response.custom_fields[i].order
            };

            for (var j = 0; j < registrantFlowService.registrantObject.registrant.custom_fields.length; j++) {
              if (registrantFlowService.registrantObject.registrant.custom_fields[j].id === response.custom_fields[i].id) {
                var customFieldValue = registrantFlowService.registrantObject.registrant.custom_fields[j].value.toString();
                attendeeFieldObject.value = customFieldValue;
                for (var k = 0; k < attendeeFieldObject.options.length; k++) {
                  if (customFieldValue && customFieldValue.indexOf(attendeeFieldObject.options[k].value) > -1) {
                    attendeeFieldObject.options[k].selected = true;
                  }
                }
              }
            }
            $scope.attendeeFields.push(attendeeFieldObject);
          }
        }

        $scope.attendeeFields  = $filter('orderBy')($scope.attendeeFields, 'order');
        var bellowStreet = false;
        for (var j = 0; j < $scope.attendeeFields.length; j++) {
          $scope.attendeeFields[j].belowStreetField = bellowStreet;
          if($scope.attendeeFields[j].code == 'street_address' && !bellowStreet && $scope.attendeeFields[j].visibility !== 'hidden'){
            bellowStreet = true;
          }
          if($scope.attendeeFields[j].code == "country") {
            $scope.countryIndex = j;
          }
          if($scope.attendeeFields[j].code == "state") {
            $scope.stateIndex = j;
          }
        }

        $timeout(function() {
          initGooglePlacesAutocomplete();
        }, 0);

        $scope.finishedLoadingData = true;
        $rootScope.showLoader      = false;
        registrantFlowService.saveRoute('attendee_info');
        $timeout(function() {
          var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
          parent.postMessage(heightToSend, '*');
        }, 0);
      }, function(error) {
        console.log(error);
      })
    }

    function initGooglePlacesAutocomplete() {
       placesInstance = new google.maps.places.Autocomplete(
          /** @type {!HTMLInputElement} */(document.getElementById('street_address')),
          {types: ['geocode']});

      placesInstance.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
      var currentPlace = placesInstance.getPlace();
      var currentPlaceComponents = currentPlace.address_components;

      for (var i = 0; i < $scope.attendeeFields.length; i++) {
        if ($scope.attendeeFields[i].code === 'country') {
          for (var j = 0; j < currentPlaceComponents.length; j++) {
            if (currentPlaceComponents[j].types.indexOf("country") > -1) {
              $scope.attendeeFields[i].value = currentPlaceComponents[j].short_name;
              for (var k = 0; k < $scope.countries.length; k++) {
                if ($scope.countries[k].code === currentPlaceComponents[j].short_name) {
                  $scope.countries[k].selected = true;
                } else {
                  $scope.countries[k].selected = false;
                }
              }
              if (currentPlaceComponents[j].short_name !== 'US') {
                $scope.attendeeFields[$scope.stateIndex].visibility = 'hidden';
              } else {
                $scope.attendeeFields[$scope.stateIndex].visibility = 'required';
              }
            }
          }
          break
        }
      }

      for (var i = 0; i < $scope.attendeeFields.length; i++) {
        for (var j = 0; j < currentPlaceComponents.length; j++) {
          if($scope.attendeeFields[i].code === 'street_address'){
            $scope.attendeeFields[i].value = currentPlace.name;
          }

          if ($scope.attendeeFields[i].code === 'city' && currentPlaceComponents[j].types.indexOf("locality") > -1) {
            $scope.attendeeFields[i].value = currentPlaceComponents[j].long_name;
          }

          if ($scope.attendeeFields[i].code === 'zip_code' && currentPlaceComponents[j].types.indexOf("postal_code") > -1) {
            $scope.attendeeFields[i].value = currentPlaceComponents[j].long_name;
          }
          if ($scope.attendeeFields[i].code === 'state' && currentPlaceComponents[j].types.indexOf("administrative_area_level_1") > -1 && $scope.attendeeFields[$scope.countryIndex].value === 'US') {
            $scope.attendeeFields[i].value = currentPlaceComponents[j].short_name;
            for (var k = 0; k < $scope.unitedStates.length; k++) {
              if ($scope.unitedStates[k].abbreviation === currentPlaceComponents[j].short_name)
              $scope.unitedStates[k].selected = true;
            }
          } else if ($scope.attendeeFields[i].code === 'state' && currentPlaceComponents[j].types.indexOf("administrative_area_level_1") > -1 && $scope.attendeeFields[$scope.countryIndex].value !== 'US') {
            $scope.attendeeFields[i].value = "";
          }
        }
      }
      $scope.$apply();
    }

    $rootScope.$on("CLOSE_DROPDOWN", function(event, data) {
      var needToResize = false;
      // for (var i = 0; i < $scope.customFields.length; i++) {
      //   if ($scope.customFields[i].openOptions) {
      //     $scope.customFields[i].openOptions = false;
      //     needToResize = true;
      //   }
      // }
      for (var i = 0; i < $scope.attendeeFields.length; i++) {
        if ($scope.attendeeFields[i].hasOwnProperty('openOptions')) {
          $scope.attendeeFields[i].openOptions = false;
          needToResize = true;
        }
      }
      if(needToResize) {
        document.getElementsByClassName('_body')[0].setAttribute('style','height:auto;');
        $timeout(function() {
         var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
         parent.postMessage(heightToSend, '*');
        }, 0);
      }
    });
    $scope.openModal = function($event) {
      $event.stopPropagation();
      $event.preventDefault();
      if($event.target.href){
        var el = document.getElementsByClassName('consent-text');
        var url = angular.element(el).find('a').attr('href');
        if(url){
          $rootScope.blurForm           = true;
          $scope.openedModal          = true;
          registrantFlowService.getFileContent(url).then(function(response) {
            $scope.fileContent = response.data.text;
          }, function(error) {
            console.log(error);
          });
        }
      }
    }

    $scope.closeModal = function($event) {
      $event.stopPropagation();
      $scope.openedModal = null;
      $rootScope.blurForm = false;
    }

    $scope.acceptTerms = function() {
        $scope.acceptedTerms = !$scope.acceptedTerms;
        //$scope.checkIfCanEnableNextButton();
    }
    $scope.load = function() {
      parent.postMessage({'task': 'form_loaded'}, '*');
    }
  }
})();
