(function(){
	'use strict'
  var serviceId		 = 'attendeeInfoDataService';
	var module 			 = angular.module('expoform.registration.attendee_info');
	var dependencies = [
		'requestService',
		'$q',
		attendeeInfoDataService
	];

	module.service(serviceId, dependencies);

	function attendeeInfoDataService(requestService, $q) {
		var service = {};

		service.getAttendeeFields = function(formId, token) {
			var deferred = $q.defer();
			var url = "forms/" + formId + "/attendee_fields/";

			requestService.get(url, token).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error)
			});
			return deferred.promise;
		}

		return service;
	}
})();
