'use strict'
var levelsModule		 		 = angular.module('expoform.registration.levels')
var dependeciesArray 					 = [
	'$stateProvider',
	'$urlRouterProvider',
	'$locationProvider',
	configLevels
];
levelsModule.config(dependeciesArray);

function configLevels($stateProvider, $urlRouterProvider, $locationProvider) {
	$stateProvider.state('registration.levels', {
		url: '/levels',
		resolve: {
			_meta: ['$rootScope', '$http', '$stateParams', function($rootScope, $http, $stateParams) {
				//get metadata for registration form
				var registrationFormId = $stateParams.id;

				return registrationFormId;
			}]
		},
		views: {
			"form": {
				templateUrl: 'app/_scenes/registration/_scenes/levels/levelsView.html',
				controller: 'levelsController'
			}
		}
	})
}
