(function(){
	'use strict'
  var serviceId		 = 'levelsDataService';
	var module 			 = angular.module('expoform.registration.levels');
	var dependencies = [
		'requestService',
		'$q',
		levelsDataService
	];

	module.service(serviceId, dependencies);

	function levelsDataService(requestService, $q) {
		var service = {};

		service.getRegistrationLevels = function(formId, token, registrant) {
			var deferred = $q.defer();
			var url = "forms/" + formId + "/registration_levels/";

			if (registrant.email_address.length > 0) {
				url += "?email_address=" + encodeURIComponent(registrant.email_address);
			}

			requestService.get(url, token).then(function(response) {
				deferred.resolve(response.data);
			}, function(error) {
				deferred.reject(error)
			});
			return deferred.promise;
		}

		return service;
	}
})();
