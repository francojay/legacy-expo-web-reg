(function () {
  'use strict';

  var controllerId = 'levelsController';
	var module 			 = angular.module('expoform.registration.levels');
	var dependecies  = [
    '$rootScope',
		'$scope',
		'$state',
		'$stateParams',
		'levelsDataService',
    'registrantFlowService',
    '$timeout',
		levelsController
	]
	module.controller(controllerId, dependecies);

  function levelsController($rootScope, $scope, $state, $stateParams, levelsDataService, registrantFlowService, $timeout) {

    var formId     = $rootScope.registration.formId;
    var cartId     = $rootScope.registration.cartId;
    var registrant = registrantFlowService.registrantObject.registrant;

    var previousSelectedLevel = registrantFlowService.registrantObject.level;

    $scope.shouldShowThePreviousButton = ($rootScope.registration.firstStep === 'attendee_first') ? true : false;

    parent.postMessage({'task': 'scroll_top'}, '*');
    registrantFlowService.facebookTrackingEvent('Expo_TypeLoad', {event: $stateParams.id});
    registrantFlowService.googleTrackingEvent({'eventCategory': 'Load',  'eventAction': 'Expo_TypeEC','eventLabel': $stateParams.id});

    getRegistrationLevels(formId, cartId, registrant);

    $scope.selectedLevel = null;

    $scope.hideErrorMessage = function($event) {
      $event.stopPropagation();
      $scope.errorMessageTop    = null;
      $scope.errorMessageBottom = null;
    }

    $scope.changeLevelSelection = function(level) {

      $scope.selectedLevel = ($scope.selectedLevel==level) ? null : level;
      if ($scope.selectedLevel != null) {
        registrantFlowService.facebookTrackingEvent('Expo_TypeSelect', {event: $stateParams.id, type: level.name, value: level.price});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_TypeEC','eventLabel': $stateParams.id});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_TypeName','eventLabel': level.name});
        registrantFlowService.googleTrackingEvent({'eventCategory': 'Select',  'eventAction': 'Expo_TypePrice','eventLabel': String(level.price)});
      }
    }

    $scope.showMoreText = function(level) {
      for (var i = 0; i < $scope.levelsList.length; i++) {
        $scope.levelsList[i].open = false;
      }

      level.open = true;
    }

    $scope.saveRegistrantLevel = function(level, clickLocation) {
      if (level) {
        registrantFlowService.setRegistrantLevel(level).then(function(registrant) {
          $scope.registrantInformationSaved = true;
          $rootScope.showLoader             = true;
          setTimeout(function () {
            $state.go('registration.sessions');
          }, 200);
        }, function(error) {
          console.log(error);
        })
        registrantFlowService.savePartialCart(formId, cartId);
      } else {
        if (clickLocation === 'top') {
          $scope.errorMessageTop = "Please select a registration type."
        } else if (clickLocation === 'bottom') {
          $scope.errorMessageBottom = "Please select a registration type."
        }
      }

    }

    $scope.navigateBack = function() {
      $scope.previousButtonDisabled = true;
      registrantFlowService.navigateBack();
    }

    function getRegistrationLevels(formId, cartId, registrant) {
      levelsDataService.getRegistrationLevels(formId, cartId, registrant).then(function(response) {
        $scope.levelsList          = response;
        if($scope.levelsList.length > 1 || $scope.levelsList.length == 0){
          $scope.finishedLoadingData = true;
          $rootScope.showLoader      = false;

          for (var i = 0; i < $scope.levelsList.length; i++) {
            if ($scope.levelsList[i].id === previousSelectedLevel) {
              $scope.selectedLevel = $scope.levelsList[i];
            }
          }
          registrantFlowService.saveRoute('levels');

          $timeout(function() {
            var heightToSend = document.getElementsByClassName('_form-wrapper')[0].offsetHeight + 47;
            parent.postMessage(heightToSend, '*');
          }, 0);
        }
        else if($scope.levelsList.length == 1){
          $scope.saveRegistrantLevel($scope.levelsList[0]);
        }
      }, function(error) {
        console.log(error);
      })
    }
  }
})();
