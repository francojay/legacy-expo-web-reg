(function() {
	'use strict'
	var registration 		 	= angular.module('expoform')
	var countries 				= require("countriesList");
	var unitedStates		  = require("unitedStatesList");
	var timezones 				= require("timezonesList");

	var detectEnvironment = require("environmentDetector");
	var urlConfigurations = require("urlConfigurations");

	registration.constant('API', {
		ENV			 			: detectEnvironment(),
		BASE_URL 			: urlConfigurations.getBaseUrl(),
		UNITED_STATES : unitedStates,
		COUNTRIES 		: countries,
		TIMEZONES			: timezones,
		STRIPE_KEY		: urlConfigurations.getStripePublishableKey()
	});
})();
