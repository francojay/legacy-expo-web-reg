function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[#?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function autoResize(frameId) {
  setSize(frameId)
}

function setSize(frameId) {
  postMessage(document.getElementById(frameId).style.height,'*');
}

var showForm = function(div, key, width) {
  var interval = setInterval(function() {
    if(document.readyState === 'complete') {
      clearInterval(interval);
      var elements = document.getElementsByClassName('expo-tickets');
      if(elements.length > 0){
          initLightboxScript(key);
          return;
      } else {
        window.scrollPositions   = [];
        window.hostPageHasLoaded = false;
        window.formHasLoaded     = false;
        window.scrollTo(0, 1);
        window.addEventListener('scroll', initScroll);

        loadJQuery(div, key, width);

        var myInterval = setInterval(function() {
          if (window.hostPageHasLoaded && window.formHasLoaded) {
            var parentWithOffset  = document.getElementById('IframeDivId').offsetParent;
            var $parentWithOffset = jQuery(parentWithOffset);

            if ($parentWithOffset[0].nodeName === "BODY") {
              var topOffset = document.getElementById('IframeDivId').offsetTop
            }  else {
              var topOffset         = $parentWithOffset.offset().top;
            }

            window.scrollPositions.push(topOffset);
            window.removeEventListener('scroll', initScroll);
            clearInterval(myInterval);
          } else {
            console.log('ceva nu e loaded');
          }
        }, 300);
      }
    }

  }, 100);

}

var loadJQuery = function(div, key, width) {
  if (typeof jQuery == 'undefined') {
    var script = document.createElement("SCRIPT");
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
    script.type = 'text/javascript';
    script.onload = function() {
      //jQuery(window).load(function() {
        window.hostPageHasLoaded = true;
        initForm(div, key, width);
    //  });
    }
    document.getElementsByTagName("head")[0].appendChild(script);
  } else {
    window.hostPageHasLoaded = true;
    initForm(div, key, width);
  }

}

var initForm = function(div, key, width) {
  var ticket = getParameterByName('ticket', location);

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function()
  {n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  if (ticket) {
    var originOfJs = document.getElementById('expo_js').src.split('/assets')[0];
      document.getElementById(div).innerHTML=
      "<iframe src=" + originOfJs + "/#!/registration/" +
key + "/step-1?ticket=" + ticket + "&callback=" + location + "' style='overflow: visible; ' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' id='expoform' allowfullscreen></iframe>";
      } else {
        var originOfJs = document.getElementById('expo_js').src.split('/assets')[0];
        var iframeHtmlText = "<iframe src='" + "https://go.regform.com/#!/registration/" + key + "/info'" + " style='width: 100%;height:100%;' onload=\"autoResize('formFrame')\" id='formFrame' allowfullscreen frameborder='0'></iframe>"
        document.getElementById(div).innerHTML= iframeHtmlText;

      }

  var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
  eventer(messageEvent,function(e) {
      if (isNaN(e.data)) {
          if (e.data['task'] == 'scroll_top') {
            var smallScreen = window.innerWidth < 891 ? true : false;
            var iframe 			= document.getElementById('IframeDivId');

            if(iframe != null){
              if (smallScreen) {
                iframe.scrollIntoView({ behavior: 'smooth', block: 'center' });
              }
            }
          }  else {
            if (e.data['task'] == 'form_loaded') {
              window.formHasLoaded = true;
            } else {
              var message = e.data;
              try {
                  var obj = JSON.parse(message);
                  if (obj.func == 'analytics' && obj.google_tracking_code) {
                      ga('create', obj.google_tracking_code, 'auto');
                      ga('send', {
                        hitType: 'event',
                        eventCategory: obj.eventCategory,
                        eventAction: obj.eventAction,
                        eventLabel: obj.eventLabel,
                        eventValue: obj.eventValue
                      });
                  }
                  if(obj.func == 'facebook' && obj.facebook_tracking_code) {
                    fbq('init', obj.facebook_tracking_code);
                    fbq('trackCustom', obj.page,obj.event_object);
                  }
                  if(obj.func == 'redirect' && obj.redirect) {
                    window.location.href = obj.redirect;
                  }
              }
              catch(err) {
                  console.log(e);
              }
            }
          }
      } else {
          var newHeight = e.data;
          document.getElementById(div).setAttribute("style","height:" + newHeight + 'px; width:' + width + 'px');
          //document.getElementById('expoform').setAttribute("style","height:" + newHeight + 'px; width:100%');
      }
  },false);
}

function initScroll() {
  if (window.scrollPositions.length > 0) {
    var output = [];
    for (var i=0; i < window.scrollPositions.length; i++) {
        if (!output[output.length-1] || output[output.length-1].value != window.scrollPositions[i])
            output.push({value: window.scrollPositions[i], times: 1})
        else
            output[output.length-1].times++;
    }
    var max = window.scrollPositions.reduce(function(a, b) {
      return Math.ceil(Math.max(a, b));
    });

    window.scrollTo(0, max);
  }
}

function initLightboxScript(key) {
  (function() {
  	var formId 	 			= key;
  	var formHasOpened = false;

  	var originalBodyOverflow 	= "";
  	var originalBodyHeight	 	= "";
  	var originalBodyWidth	 		= "";
  	var originalBodyMaxHeight = "";
  	var originalBodyPosition 	= "";

  	var originalDocumentOverflow 	= "";
  	var originalDocumentHeight 		= "";
  	var originalDocumentMaxHeight = "";
  	var originalDocumentPosition 	= "";

  	var elementPosition = "";

  	window.initExpo = function(id) {
  		formId = id;
  	}

  	var interval = setInterval(function() {
  	  if(document.readyState === 'complete') {
  	    clearInterval(interval);
  	    addLightbox();
  			checkTicketState();
  			window.onresize = initResizeBehaviour;
  	  }
  	}, 100);

  	function addLightbox() {
  		var lighboxDiv		 			 = document.createElement('div');
  		var lighboxStyling 			 = "position: fixed;top: 0px;left: 0px;right: 0px;z-index:1000000;width: 100%;height: 100%;bottom: 0px;background-color: rgba(0, 0, 0, 0.8);visibility:hidden; opacity: 0; transition:transform .3s ease, visibility .3s ease, opacity .3s ease;";
  		lighboxDiv.style.cssText = lighboxStyling;
  		lighboxDiv.id 					 = "expo_lightbox";

  		var lightboxWrapStyle 				= "height: 100%;overflow: scroll;position:absolute;width:100%;";
  		var lightboxWrapDiv	  				= document.createElement('div');
  		lightboxWrapDiv.id						= "expo_lightbox_wrap";
  		lightboxWrapDiv.style.cssText = lightboxWrapStyle;
  		lighboxDiv.appendChild(lightboxWrapDiv);

  		var lightboxLoader = document.createElement('div');
  		lightboxLoader.id	 = "expo_lightbox_loader";
  		lightboxWrapDiv.appendChild(lightboxLoader);

  		var closeIconWrapper 					 = document.createElement('div');
  		closeIconWrapper.id 					 = 'close_expo_lightbox';
  		closeIconWrapper.style.cssText = "position: relative;top: 20px; left:calc(50vw + 440px);transition:translate(20%);cursor: pointer; opacity: 0.8; transition: opacity .3s ease;width: 32px;height: 32px;z-index: 9999999999;visibility:hidden;";

  		var styleEl = document.createElement('style');
  	  // Append <style> element to <head>
  	  document.head.appendChild(styleEl);

  	  // Grab style element's sheet
  	  var styleSheet = styleEl.sheet;


  		if("insertRule" in styleSheet) {
  			var rules = styleSheet.cssRules;
  			styleSheet.insertRule("#close_expo_lightbox:before { position: absolute;left: 15px;top:8px;content: ' ';height: 20px;width: 2px;background-color: #fff;transform: rotate(45deg) }", 0);
  			styleSheet.insertRule("#close_expo_lightbox:after { position: absolute;left: 15px;top:8px;content: ' ';height: 20px;width: 2px;background-color: #fff;transform: rotate(-45deg) }", 0);
  			styleSheet.insertRule('#expo_lightbox { -webkit-overflow-scrolling: touch;}', rules.length ? rules.length : 0);
  			styleSheet.insertRule('#expo_lightbox_loader { display: inline-block;width: 50px;height: 50px;border: 3px solid rgba(255,255,255,.3);border-radius: 50%;border-top-color: #fff;animation: spin 1s ease-in-out infinite;-webkit-animation: spin 1s ease-in-out infinite;position: absolute;top: 50vh;left: 50vw;}', rules.length ? rules.length : 0);
  			if (CSSRule.KEYFRAMES_RULE) { // W3C
  			    styleSheet.insertRule("@keyframes spin {\ to {\ -webkit-transform: rotate(360deg); }\ }", rules.length ? rules.length : 0);
  			} else if (CSSRule.WEBKIT_KEYFRAMES_RULE) { // WebKit
  			    styleSheet.insertRule("@-webkit-keyframes spin {\ to {\ -webkit-transform: rotate(360deg); }\ }", rules.length ? rules.length : 0);
  			}
  		}
  		else if("addRule" in styleSheet) {
  			styleSheet.addRule("#close_expo_lightbox:before","position: absolute;left: 15px;top:8px;content: ' ';height: 20px;width: 2px;background-color: #fff;transform: rotate(45deg);");
  			styleSheet.addRule("#close_expo_lightbox:after","position: absolute;left: 15px;top:8px;content: ' ';height: 20px;width: 2px;background-color: #fff;transform: rotate(-45deg);");
  			styleSheet.addRule('#expo_lightbox','-webkit-overflow-scrolling: touch;');
  			styleSheet.addRule('#expo_lightbox_loader ', ' display: inline-block;width: 50px;height: 50px;border: 3px solid rgba(255,255,255,.3);border-radius: 50%;border-top-color: #fff;animation: spin 1s ease-in-out infinite;-webkit-animation: spin 1s ease-in-out infinite;position: absolute;top: 50vh;left: 50vw;');
  			styleSheet.addRule('@keyframes spin ', ' to { -webkit-transform: rotate(360deg); } ');
  			styleSheet.addRule('@-webkit-keyframes spin' ,'  to { -webkit-transform: rotate(360deg); } ');
  		}


  		lightboxWrapDiv.appendChild(closeIconWrapper);

  		document.body.appendChild(lighboxDiv);

  		initClickListeners();
  	}

  	function showLightbox() {
  		formIsOnScreen 	 = true;
  		var ticket 			 = getParameterByName('ticket', location);

  		//elementPosition = document.getElementsByClassName('expo-tickets').getBoundingClientRect();
  		originalBodyOverflow 	= document.body.style.overflow;
  		originalBodyHeight	 	= document.body.style.height;
  		originalBodyHeight	 	= document.body.style.width;
  		originalBodyMaxHeight = document.body.style.maxHeight;
  		originalBodyPosition 	= document.body.style.position;

  		originalDocumentOverflow 	= document.documentElement.style.overflow;
  		originalDocumentHeight 		= document.documentElement.style.height;
  		originalDocumentMaxHeight = document.documentElement.style.maxHeight;
  		originalDocumentPosition 	= document.documentElement.style.position;


  		document.body.style.setProperty("overflow", "hidden", "important");
  		document.body.style.setProperty("height", "100%", "important");
  		document.body.style.setProperty("width", "100%", "important");
  		document.body.style.setProperty("max-height", "100%", "important");
  		document.body.style.setProperty("position", "fixed", "important");


  		document.documentElement.style.setProperty("overflow", "hidden", "important");
  		document.documentElement.style.setProperty("height", "100%", "important");
  		document.documentElement.style.setProperty("max-height", "100%", "important");
  		document.documentElement.style.setProperty("position", "fixed", "important");


  		var iframeSource = "";
  		if (!ticket) {
  			iframeSource	 = "<iframe src='https://go.regform.com/#!/registration/" + formId + "/info"  + "'style='min-height: 400px; width: 881px; height: 100%;left: 50.5%;transform: translateX(-50.5%) translateZ(0);position: relative;' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' id='expoframe'></iframe>";
  		} else {
  			iframeSource	 = "<iframe src='https://go.regform.com/#!/registration/" + formId + "/info" + "'style='min-height: 400px; width: 881px; height: 100%;left: 50.5%;transform: translateX(-50.5%) translateZ(0);position: relative;' frameborder='0' scrolling='yes'  marginheight='0' marginwidth='0' id='expoframe'></iframe>";
  		}

  		var lightbox = document.getElementById('expo_lightbox');
  		lightbox.style.visibility = 'visible';
  		lightbox.style.transform  = 'scale(1)';
  		lightbox.style.opacity 		= 1;
  		var closeIconWrapper = document.getElementById('close_expo_lightbox');
  		closeIconWrapper.style.visibility = 'visible';
  		closeIconWrapper.style.transform  = 'scale(1)';
  		closeIconWrapper.style.opacity 		= 1;
  		var lightboxWrapDiv = document.getElementById('expo_lightbox_wrap');
  		if (!formHasOpened) {
  			lightboxWrapDiv.innerHTML += iframeSource;
  		}

  		formHasOpened = true;

  		initClickListeners();
  		initResizeBehaviour();
  	}

  	function hideLightbox() {
  		formIsOnScreen = false;
  		var lightbox = document.getElementById('expo_lightbox');
  		lightbox.style.visibility = 'hidden';
  		lightbox.style.transform  = 'scale(0)';
  		lightbox.style.opacity 		= 0;
  		var closeButton = document.getElementById('close_expo_lightbox');
  		closeButton.style.visibility = 'hidden';
  		closeButton.style.transform  = 'scale(0)';
  		closeButton.style.opacity 		= 0;

  		document.body.removeAttribute("style");
  		//document.body.style.removeProperty("height");
  		document.documentElement.removeAttribute("style");
  		//document.documentElement.style.removeProperty("height");

  		document.body.style.setProperty("overflow", originalBodyOverflow);
  		document.body.style.setProperty("height", originalBodyHeight);
  		document.body.style.setProperty("width", originalBodyWidth);
  		document.body.style.setProperty("max-height", originalBodyMaxHeight);
  		document.body.style.setProperty("position", originalBodyPosition);


  		document.documentElement.style.setProperty("overflow", originalDocumentOverflow);
  		document.documentElement.style.setProperty("height", originalDocumentHeight);
  		document.documentElement.style.setProperty("max-height", originalDocumentMaxHeight);
  		document.documentElement.style.setProperty("position", originalDocumentPosition);


  		document.getElementsByClassName('expo-tickets')[0].scrollIntoView({ behavior: 'smooth', block: 'center' });
  	}

  	function initClickListeners() {
  		var elements = document.getElementsByClassName('expo-tickets');

  		for (var i = 0; i < elements.length; i++) {
      	elements[i].addEventListener('click', showLightbox, false);
  		}

  		var lightbox = document.getElementById('expo_lightbox');
  		lightbox.addEventListener('click', hideLightbox, false);


  		var closeButton = document.getElementById('close_expo_lightbox');
  		closeButton.addEventListener('click', hideLightbox, false);

  		closeButton.addEventListener('mouseover', function() {
  			closeButton.style.opacity = 1;
  		}, false);

  		closeButton.addEventListener('mouseout', function() {
  			closeButton.style.opacity = 0.8;
  		}, false);



  		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  	  !function(f,b,e,v,n,t,s)
  	  {if(f.fbq)return;n=f.fbq=function()
  	  {n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  	  n.queue=[];t=b.createElement(e);t.async=!0;
  	  t.src=v;s=b.getElementsByTagName(e)[0];
  	  s.parentNode.insertBefore(t,s)}(window,document,'script',
  	  'https://connect.facebook.net/en_US/fbevents.js');
  		var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  	  var eventer = window[eventMethod];
  	  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
  	  eventer(messageEvent,function(e) {
  	      if (!isNaN(e.data)) {
  					var newHeight = e.data;
  					var iframe 			= document.getElementById('expoframe');
  					if(iframe != null){
  						iframe.style.height = newHeight + "px";
  						var lightboxLoader = document.getElementById('expo_lightbox_loader');
  						lightboxLoader.style.display = "none";
  					}
  	      }
  				else {
  					var message = e.data;
  					try {
  						if (e.data['task'] == 'scroll_top') {
  							var smallScreen = window.innerWidth < 891 ? true : false;
  							var iframe 			= document.getElementById('expoframe');

  								if(iframe != null){
  									if (smallScreen) {
  										iframe.scrollIntoView({ behavior: 'smooth', block: 'center' });
  									}
  								}

  							} else if(e.data['task'] == 'redraw') {
                  var iFrameElement = document.getElementById('expoframe');
                  if (iFrameElement) {
                    var oldVal = "translateX(-50.5%)";
                    iFrameElement.style.transform = "translateX(-50.5%) translateZ(1px)";
                    setTimeout(function() {
                      iFrameElement.style.transform = oldVal;
                    }, 0);
                  }
                } else if(e.data['task'] == 'close_lightbox') {
                  hideLightbox();
                }
  							var obj = JSON.parse(message);
  							if (obj.func == 'analytics' && obj.google_tracking_code) {
  									ga('create', obj.google_tracking_code, 'auto');
  									ga('send', {
  										hitType				: 'event',
  										eventCategory : obj.eventCategory,
  										eventAction		: obj.eventAction,
  										eventLabel		: obj.eventLabel,
  										eventValue		: obj.eventValue
  									});
  							}
  							if(obj.func == 'facebook' && obj.facebook_tracking_code) {
  								fbq('init', obj.facebook_tracking_code);
  								fbq('trackCustom', obj.page,obj.event_object);
  							}

                if(obj.func == 'redirect' && obj.redirect) {
                  window.location.href = obj.redirect;
                }
  					}
  					catch(err) {
  					}
  				}
  	  },false);
  	}



  	function getParameterByName(name, url) {
  	    if (!url) url = window.location.href;
  	    name = name.replace(/[\[\]]/g, "\\$&");
  	    var regex 	= new RegExp("[#?&]" + name + "(=([^&#]*)|&|#|$)");
  			var results = regex.exec(url);

  			if (!results) return null;

  			if (!results[2]) return '';

  			return decodeURIComponent(results[2].replace(/\+/g, " "));
  	}

  	function initResizeBehaviour() {
  		var smallScreen = window.innerWidth < 891 ? true : false;
  		var iframe 			= document.getElementById('expoframe');
  		var lightbox = document.getElementById('expo_lightbox');
  		var closeButton = document.getElementById('close_expo_lightbox');

  		if(iframe != null) {
  			if (smallScreen) {
  				iframe.style.width		 = '90%';
  				lightbox.style.webkitOverflowScrolling =  "touch";
  				iframe.style.marginTop = "20px";
  				closeButton.style.left							 = "50.5%";
  				closeButton.style.transform					 = "translateX(-50.5%)";
  				closeButton.style.top 						 	 = "0px";

  			} else {
  				iframe.style.marginTop = "40px";
  				iframe.style.marginBottom = "100px";
  				iframe.style.width		 = '881px';
  			}
  		}
  	}

  	function checkTicketState() {
  		var ticket 			 = getParameterByName('ticket', location);
  		if (ticket) {
  			showLightbox()
  		}
  	}

  })();

}
