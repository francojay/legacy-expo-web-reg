(function(){
	'use strict'

  var serviceId		 = 'redrawService';
	var module 			 = angular.module('expoform.services');
	var dependencies = [
		redrawService
	]

	module.service(serviceId, dependencies);

	function redrawService() {
		var service = {};

		service.forceRedraw = function() {
			parent.postMessage({'task': 'redraw'}, '*');
			
		}

		return service;
	}
})();
