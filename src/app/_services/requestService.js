(function(){
	'use strict'

  var serviceId		 = 'requestService';
	var module 			 = angular.module('expoform.services');
	var dependencies = [
		'$q',
		'$http',
		'API',
		requestService
	]

	module.service(serviceId, dependencies);

	function requestService($q, $http, API) {

		var service = {};

		service.get = function(url, token) {
			url = API.BASE_URL + url;
			var deferred = $q.defer();
			if (token) {
				var headers = {
					'Authorization': token,
					'Accept': '*/*'
				}
			} else {
				var headers = {
					'Accept': '*/*'
				}
			}

			$http.get(url, {
				'headers': headers
			}).then(function(response) {
				deferred.resolve(response);
			}, function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.post = function(url, data, token) {
			url 				 = API.BASE_URL + url;
			var deferred = $q.defer();

			if (token) {
				var headers = {
					'Authorization': token,
					'Accept': '*/*'
				}
			} else {
				var headers = {
					'Accept': '*/*'
				}
			}

			$http.post(url, data, {
				"headers": headers
			}).then(function(response) {
				deferred.resolve(response);
			}, function(error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}


		service.delete = function(url, token) {
			url 				 = API.BASE_URL + url;
			var deferred = $q.defer();

			if (token) {
				var headers = {
					'Authorization': token,
					'Accept': '*/*'
				}
			} else {
				var headers = {
					'Accept': '*/*'
				}
			}

			$http.delete(url, {
				'headers': headers
			}).then(function(response) {
				deferred.resolve(response);
			}, function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		service.readFileContent = function(url) {
			var deferred = $q.defer();

			$http.get(url,{transformResponse: [function (data) {
				var newData = {
					text: data
				}
				return newData;
			}]}).then(function(response) {
				deferred.resolve(response);
			}, function (error) {
				deferred.reject(error);
			});

			return deferred.promise;
		}

		return service;
	}
})();
