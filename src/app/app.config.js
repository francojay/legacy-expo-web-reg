(function(){
	'use strict'
	var expoform		 		 = angular.module('expoform');
	var dependeciesArray = ['$stateProvider', '$urlRouterProvider', '$locationProvider', 'API', configExpoform]

	expoform.config(dependeciesArray);

	function configExpoform($stateProvider, $urlRouterProvider, $locationProvider, API) {
		var currentRegistrationId =  localStorage.getItem('expo.registration_form_id');
		$locationProvider.hashPrefix("!");

		$stateProvider
			.state('registration', {
				url: '/registration/:id',
				resolve: {
					_meta: ['$rootScope', '$http', '$stateParams', 'requestService', 'API', function($rootScope, $http, $stateParams, requestService, API) {
						var activeExpoforms 			= JSON.parse(localStorage.getItem('active_expoforms'));
						var formId 								= new Hashids('Expo', 7).decode($stateParams.id)[0];
						var url 									= 'forms/' + formId + '/meta/';

						var thisFormExistsLocally = false;
						var currentFormData 			= {};
						var formIndex							= null;
						for (var i = 0; i < activeExpoforms.length; i++) {
							if (activeExpoforms[i].formId === formId && activeExpoforms[i].conferenceId != null && activeExpoforms[i].cartId != null) {
								var needsFormReload 	= false;
								thisFormExistsLocally = true;
								formIndex							= i;
								currentFormData 			= activeExpoforms[i];

								if (currentFormData.savedTime && moment(currentFormData.savedTime).add(3, 'hours').isBefore(moment())) {
									needsFormReload = true;
								} else if (!currentFormData.savedTime) {
									needsFormReload = true;
								}
							}
	 					}
						if (!thisFormExistsLocally || needsFormReload) {
							return requestService.get(url, false).then(function(response) {
								var formObject = {
									formId 			 		 		 : formId,
									conferenceId 		 		 : response.data.conference_id,
									cartId 			 		 		 : thisFormExistsLocally ? currentFormData.cartId : response.data.cart_id,
									firstStep 	 		 		 : response.data.flow_type,
									consent 				 		 : response.data.registration_p1_policy,
									conferenceStartDate  : response.data.date_from,
									conferenceEndDate	   : response.data.date_to,
									paymentProvider 		 : response.data.payment_provider,
									paymentTypes				 : response.data.payment_types,
									conferenceTimezone	 : response.data.timezone_name,
									terms_of_service		 : response.data.terms_of_service,
									refund_policy				 : response.data.refund_policy,
									googleTrackingCode	 : response.data.google_tracking_code,
									facebookTrackingCode : response.data.facebook_tracking_code,
									redirectUrlDone			 : response.data.redirect_url || null,
									savedTime						 : moment(),
									cart 					 	 		 : thisFormExistsLocally ? currentFormData.cart : [],
								}

								if(!thisFormExistsLocally) {
									activeExpoforms.push(formObject);
								} else {
									activeExpoforms[formIndex] = formObject;
								}

								localStorage.setItem('active_expoforms', JSON.stringify(activeExpoforms));
								currentFormData 			= formObject
								return currentFormData;
							}, function(error) {
								if (error.status === 404) { //form not found
									alert("The form that you tried loading doesn't exist!");
								}
								else {
									$rootScope.errorMessage = error.data.detail;
								}
							});
						} else {
							return currentFormData;
						}
					}]
				},
				views: {
					"app": {
						templateUrl: 'app/_scenes/registration/registrationView.html',
						controller: 'registrationController'
					}
				}
			})
			.state('preregistration', {
				url: '/preregistration/:ticket',
				resolve: {
					_meta: ['$rootScope', '$http', '$stateParams', 'requestService', function($rootScope, $http, $stateParams, requestService) {
						//get metadata for pre-registration
						var preregitrationTicketId = $stateParams.ticket;
						var url 									= 'preregistration/meta/';
						return requestService.get(url, preregitrationTicketId).then(function(response) {
							var formObject = {
								conferenceStartDate : response.data.date_from,
								conferenceEndDate	  : response.data.date_to,
								conferenceTimezone	: response.data.timezone_name,
								sessions						: response.data.sessions,
								educations          : response.data.educations,
								preregitrationTicketId : preregitrationTicketId
							}
							return formObject;
						}, function(error) {
							if (error.status === 404) { //form not found
								alert("The form that you tried loading doesn't exist!");
							}
						});
					}]
				},
				views: {
					"app" :{
						templateUrl: 'app/_scenes/preregistration/preregistrationView.html',
						controller: 'preregistrationController'
					}
				}
			})

	}

	expoform.run(function($rootScope) {
		var formsArray = localStorage.getItem('active_expoforms');
		if (!formsArray) {
			localStorage.setItem('active_expoforms', JSON.stringify([]));
		}


	})
})();
