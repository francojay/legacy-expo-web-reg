'use strict';

var gulp = require('gulp');
var ignore = require('gulp-ignore');

var paths = gulp.paths;

var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/**/*.html',
    paths.tmp + '/{app}/**/*.html'
  ])
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'expoform.templates'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(paths.tmp + '/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: true
  };

  var htmlFilter = $.filter('*.html');
  var jsFilter = $.filter('**/*.js');
  var cssFilter = $.filter('**/*.css');
  var imgFilter = $.filter('_assets/**');
  var assets;

  return gulp.src(paths.tmp + '/serve/*.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe(assets = $.useref.assets())
    .pipe($.rev())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe(ignore.exclude([ "**/*.map" ]))
     .pipe($.uglify(/*{preserveComments: $.uglifySaveLicense}*/))
    .on('error', function(error){
      console.log(error);
    })
    .pipe(jsFilter.restore())
    // .pipe(imgFilter)
    // .pipe(imgFilter.restore())
    .pipe(cssFilter)
    .pipe($.csso())
    .pipe(cssFilter.restore())
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(htmlFilter.restore())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/app/_assets/images/**/*')
    .pipe(gulp.dest(paths.dist + '/app/_assets/images/'));
});

gulp.task('custom_fonts', function () {
  return gulp.src(paths.src + '/assets/fonts/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/fonts/'));
});

gulp.task('custom_scripts', function () {
  return gulp.src(paths.src + '/app/_assets/integration/**/*')
    .pipe(gulp.dest(paths.dist + '/_assets/scripts/'));
});

gulp.task('components', function () {
  return gulp.src(paths.src + '/assets/components/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/components/'));
});

gulp.task('demos', function () {
  return gulp.src(paths.src + '/assets/demo/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/demo/'));
});

gulp.task('sounds', function () {
  return gulp.src(paths.src + '/assets/sounds/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/sounds/'));
});

gulp.task('robots', function () {
  return gulp.src(paths.src + '/**/robots.txt')
    .pipe(gulp.dest(paths.dist + '/'));
});
gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/webfonts/'));
});

gulp.task('misc', function () {
  return gulp.src(paths.src + '/**/*.ico')
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('clean', function (done) {
  $.del([paths.dist + '/', paths.tmp + '/'], done);
});

gulp.task('build', ['html', 'images', 'custom_fonts', 'custom_scripts', 'robots', 'sounds', 'fonts', 'misc', 'demos',
  'components']);
